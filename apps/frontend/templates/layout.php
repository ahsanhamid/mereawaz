<?php
$action = $sf_context->getActionName();
$module = $sf_context->getModuleName();

?>
<!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href='http://fonts.googleapis.com/css?family=Lato:400,900italic,900,700italic,700,400italic,300,300italic,100italic,100' rel='stylesheet' type='text/css'>
        <?php include_http_metas() ?>
		<?php include_metas() ?>
        <?php include_title() ?>
        <link rel="shortcut icon" href="<?php echo image_path('favicon01.png');?>" />
        <?php include_stylesheets() ?>
        <?php include_javascripts() ?>       
    </head>
  <body>
  	<header class="sticky">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-3 col-xs-12">
                  <a class="logo" href="<?php echo url_for('@homepage'); ?>"><img src="<?php echo image_path('logo01.png'); ?>" alt="logo"/></a>
                  <!--<div class="navbar-header">-->
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                        </button>
                 <!-- </div>-->
                </div>
                <?php include_component('main', 'dynamicMenu'); ?>
                <div class="clearfix"></div>
           </div>
        </div>
    </header>   
        <!--header end-->
    <?php echo $sf_content ?>
            <!--footer strat-->
    <footer >
        <div class="container">
        <div class="foot">
            <p>All rights reserved . <a class="copyright" href="<?php echo url_for('@homepage'); ?>">Mere Awaz <?php echo date('Y')?> </a></p>
        </div>
        <div class="social-icone">
        <li><a class="apple" href="https://itunes.apple.com/us/genre/music/id34" target="_blank"><!--<img src="images/Apple-Icon.png" alt=""/>--></a></li>
        <li><a class="android" href="https://play.google.com/store?hl=en" target="_blank"><!--<img src="images/android-logo-black-and-white-261x300.png" alt="linkdin"/>--></a></li>
        <li><a class="twitter" href="http://www.twitter.com" target="_blank"><!--<img src="images/Twitter-icon.png" alt="linkdin"/>--></a></li>
        <li><a class="facebook" href="http://www.facebook.com" target="_blank"><!--<img src="img/facebook.jpg" alt="facebook"/>--></a></li>
        <li><a class="linkdin" href="http://www.linkedin.com" target="_blank"><!--<img src="img/linkdin.jpg" alt="linkdin"/>--></a></li>
        </div>
        </div>
    </footer>
  </body>
</html>
