<div class="section-banner section_change_margin">
  <div class="container-fluid banner_bg">
    <div class="row">
      <div class="header-banner">
        <ul>
        	<li>
            	<span><img src="<?php echo image_path('ios.png'); ?>" alt="#" /></span>
                <a href="#"><img src="<?php echo image_path('ios_btn.png');?>" alt="#" /></a>
            </li>
            <li>
            	<span><img src="<?php echo image_path('android.png');?>" alt="#"/></span>
                <a href="#"><img src="<?php echo image_path('android_btn.png');?>" alt="#"/></a>
            </li>
        </ul>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<section>
  	<div class="section-top">
			 <!--section inquery-->
     		 <div class="inquery">
            	<div class="container">
                     <div class="row">
                        <h1>Complaints</h1>
                    </div>
               		 <div class="row">
                     	<?php if($complaints){
							foreach($complaints as $comp){?>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-12">
                                <div class="inquery-sec-shap">
                                    <div class="inquery-img">
                                    <a href="<?php echo url_for('@complaint?id='.$comp->getId());?>">
                                    <?php $temp_file = $comp->getFilePath();
									$temp_file_type = $comp->getFileType();
									if($temp_file_type == "image"){
										if(isset($temp_file) && $temp_file != ''){?>
											<img src="<?php echo image_path('/uploads/complaints/'.$comp->getId().'/'.$comp->getFileType().'/'.$temp_file); ?>" alt="complaint-img"/>
										<?php }else{?>
											<img src="<?php echo image_path('no-img.png'); ?>" alt="inquery-img"/>
										<?php }?>
                                     <?php }else if($temp_file_type == "audio"){?>
                                     <img src="<?php echo image_path('audio.png'); ?>" alt="inquery-img"/>
                                     <?php }else if($temp_file_type == "video"){?>
                                     <img src="<?php echo image_path('video.png'); ?>" alt="inquery-img"/>
                                     <?php }else{?>
                                     	<img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
									 <?php }?>
                                    </a>
                                    </div>
                                </div>
                                <div class="inquery-sec-text">
                                    <h3>
                                    	<a href ="<?php echo url_for('@complaint?id='.$comp->getId());?>">
											<?php $temp_cat = $comp->getCatId(); 
											if($temp_cat < 6){
												echo $comp->getIssueName();
											}else if($temp_cat == 6){
												echo $comp->getVictimName();
											}else if($temp_cat == 7){
												echo $comp->getOverseaName();
											}
                                           ?>     
                                     	</a>
                                    </h3>
                                    <ul>
                                    	<li>
                                        	<span><?php echo $comp->getCategory()->getName();?> </span>
                                        </li>
                                        <li>
                                        	<span><?php echo $comp->getPlace()->getName();?></span>
                                        </li>
                                        <li>
                                        	<span><?php echo $comp->getIssue()->getName();?></span>
                                        </li>
                                    </ul>
                                </div>
    
                        	<div class="inquery-seemore-btn">
	                            <a href="<?php echo url_for('@category?cat='.$comp->getCategory()->getId());?>">See More</a>
    	                    </div>
                        </div>
                        <?php }
						}?>
                	</div>
             </div>
        </div>  
  </div>
</section>