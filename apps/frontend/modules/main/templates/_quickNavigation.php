<div class="panel-group" role="tablist">
<?php //$counter = 1; $sub_count = 101; $exten_sub_count = 201;
foreach($categories as $cat){?>
	 <div class="panel panel-default">
		
		<?php $catPlaces = $cat->getCategoryPlaces();
		
		if($catPlaces){?>
		 <div id="categoryID<?php echo $cat->getId();?>" class="panel-heading" role="tab">
			<h4 class="panel-title">
			 <a class="collapsed" 
			 aria-controls="categoryPlace<?php echo $cat->getId();?>" 
			 aria-expanded="false" 
			 href="#categoryPlace<?php echo $cat->getId();?>" 
			 data-toggle="collapse" role="button">
				<span class="caret">&nbsp;</span><?php echo $cat->getName();?></a>
			</h4>
		</div>
		
		<div 
		id="categoryPlace<?php echo $cat->getId();?>" 
		class="panel-collapse collapse" 
		aria-labelledby="categoryID<?php echo $cat->getId();?>" 
		role="tabpanel" 
		aria-expanded="false" style="height: 0px;">
			<ul class="list-group">
		<?php foreach($catPlaces as $cp){
			$tempCatPlaceId = $cp->getPlaceId();
			if($tempCatPlaceId != 42){?>
	   
		
				<li class="list-group-item tab-side">
					<a  class="collapsed" 
					aria-controls="placeIssue<?php echo $cp->getPlaceId();?>" 
					aria-expanded="false" 
					href="#placeIssue<?php echo $cp->getPlaceId();?>" 
					data-toggle="collapse" role="button">
						<span class="caret"></span> <?php echo $cp->getPlace()->getName();?>
					 </a>
				 </li>
					
					<?php $placeIssues = $cp->getPlace()->getPlaceIssues();
					
					if($placeIssues){?>
					<div 
					id="placeIssue<?php echo $cp->getPlaceId();?>" 
					class="panel-collapse collapse" 
					aria-labelledby="categoryPlaceGroup<?php echo $cp->getPlaceId();?>" 
					role="tabpanel" aria-expanded="false" style="height: 0px;">
						<?php foreach($placeIssues as $pi){?>
					 
						 <li class="list-group-item tab-side-child" id="sub_element<?php echo $cp->getPlaceId()."_".$pi->getIssueId();?>"><a href="<?php echo url_for('main/category?iid='.$pi->getId());?>" class="abc"><?php echo $pi->getIssue()->getName()?></a></li>
					
					 <?php 
					 }?>
					  </div>
					<?php }?>
					
			
		<?php }else{?>
		<li class="list-group-item no-place"><a href="<?php echo url_for('main/category?pid='.$cp->getId());?>"><span><?php echo $cp->getPlace()->getName()?></span></a></li>
        <div id="placeIssue<?php echo $cp->getPlaceId();?>"></div>
		<?php }
		}?>
		</ul>
		</div>
		<?php }?>
	 </div>
	 <?php  }?>
  </div>
  <script type="text/javascript">
  	$(document).ready(function(){
		$('#categoryPlace<?php echo $selected_cat;?>').collapse('show');
		$('#categoryID<?php echo $selected_cat;?> h4 a span').addClass('caretup');
		<?php if($selected_place != 0 && $selected_place != 42){?>
	    $('#placeIssue<?php echo $selected_place;?>').prev().children(0).children(0).addClass('caretup');		
		<?php }?>
		
		<?php if($selected_place != 42){?>
		$('#placeIssue<?php echo $selected_place;?>').collapse('show');
		$("#sub_element<?php echo $selected_place."_".$selected_issue;?>").addClass("quick_navigation_active");
		<?php }else{?>
			$('#categoryPlace<?php echo $selected_cat;?> #placeIssue<?php echo $selected_place;?>').prev().addClass("quick_navigation_active");
		<?php }?>
	});
  </script>