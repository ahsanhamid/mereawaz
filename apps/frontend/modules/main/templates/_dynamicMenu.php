<div class="col-lg-8 col-md-8 col-sm-9 col-xs-12">
  <nav class="navbar navbar-default nav">
      
      <div class="collapse navbar-collapse"  id="bs-example-navbar-collapse-1">
           <div class="menu">
                <ul>
                      <li class="<?php $action_temp = $sf_context->getActionName(); echo (($action_temp == 'index')?'active':'');?>">
                          <a href="<?php echo url_for('@homepage'); ?>">Home</a>
                      </li>
                      <li class="<?php $action_temp = $sf_context->getActionName(); echo (($action_temp == 'allCategory')?'active':'');?>">
                          <a href="<?php echo url_for('@all_category'); ?>" >Categories</a>
                          <!--<ul>-->
                          	<?php //foreach($categories as $cat){?>
                              <!--<li><a href="#" ><span>&nbsp;</span><?php //echo $cat->getName();?></a></li>-->
							<?php //}?>
                          <!--</ul>-->
                       </li>
                       <li class="<?php $action_temp = $sf_context->getActionName(); echo (($action_temp == 'category' || $action_temp == 'complaint')?'active':'');?>">
                          <a href="<?php echo url_for('@category'); ?>">Search</a>
                      </li>
                      <!--<li>
                          <a href="detail.html" >Detaile</a>
                      </li>-->
                      <li class="<?php $action_temp = $sf_context->getActionName(); echo (($action_temp == 'contactUs')?'active':'');?>">
                          <a href="<?php echo url_for('@contactus');?>" >Contact</a>
                      </li>
                      <li class="<?php $action_temp = $sf_context->getActionName(); echo (($action_temp == 'aboutUs')?'active':'');?>">
                          <a href="<?php echo url_for('@aboutus');?>" >About</a>
                      </li>
                  </ul>
           </div>
      </div>
  </nav>
  
</div>