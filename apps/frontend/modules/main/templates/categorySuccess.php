<div class="section-banner less_padding"> &nbsp; </div>
<section>
  	<div class="section-top">
     		 <div class="container">
    			<div class="row">
                     <div class="top-stion-text">
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-12">
                          	 <?php include_component('main', 'quickNavigation'); ?>	
                         </div>
                        <div class="col-lg-9 col-md-9 col-sm-8 col-xs-12">
                        <form action="<?php echo url_for('main/category');?>" method="post">
                        <div class="searchby_form">
                          <div class="form-group change_widthtwo">
                            <label>Search By:</label>
                          </div>
                          <div class="form-group change_width">
                          <?php if($filter_province){?>
                            <select class="form-control" id="filter_province" name="filter_province">
								<option value="0">All</option>
								<?php foreach($filter_province as $fp){?>
									<option value="<?php echo $fp->getId();?>" <?php echo (($selected_province == $fp->getId())?'selected="selected"':'')?>><?php echo $fp->getName();?></option>
								<?php }?>
                            </select>
                            <?php }?>
                          </div>
                          <div class="form-group change_width">
                          <?php if($categories){?>
                            <select class="form-control" id="cat" name="cat">
                            <option value="0">All</option>
							<?php  foreach($categories as $fc){?>
                              <option value="<?php echo $fc->getId();?>" <?php echo (($selected_cat == $fc->getId())?'selected="selected"':'')?>><?php echo $fc->getName();?></option>
                            <?php }?>
                            </select>
                            <?php }?>
                          </div>
                          <div class="form-group change_width">
                           <input class="form-control" type="text"  id="from_date" name="from_date" placeholder="From" readonly value="<?php echo ((isset($selected_from_date) && $selected_from_date != 0)?$selected_from_date:'');?>" />
                          </div>
                          <div class="form-group change_width">
                            <input class="form-control" type="text"  id="to_date" name="to_date" placeholder="To" readonly value="<?php echo ((isset($selected_to_date) && $selected_to_date != 0)?$selected_to_date:'');?>" />
                          </div>
                          <div class="clearfix"></div>
                          <div class="form-group change_width btn_serrch_space">
                            <input class="btn_search" type="submit" value="Search"/>
                          </div>
                          <div class="form-group change_width"> <a href="javascript:void(0);" class="reset_btn">Reset</a> </div>
                          <div class="clearfix"></div>
                        </div>
                        </form>
                            <div class="gallery">
                            	<?php if($complaints){ 
									foreach($complaints as $cat){?>
                                        <li>
                                        	<a rel="example_group" href="<?php echo url_for('@complaint?id='.$cat->getId()); ?>" title="<?php echo $cat->getIssueName();?>">
                                            	<?php $temp_img = $cat->getFilePath();
												$temp_type = $cat->getFileType();
												if($temp_type == "image"){
													if(isset($temp_img) && $temp_img != ''){?>
														<img src="<?php echo image_path('/uploads/complaints/'.$cat->getId().'/'.$temp_type.'/'.$temp_img); ?>" alt="<?php echo $cat->getIssueName();?>" />
													<?php }else{?>
														<img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
													<?php }?>
                                                 <?php }else if($temp_type == "audio"){?>
                                                 <img src="<?php echo image_path('audio.png'); ?>" alt="no-img" />
                                                 <?php }else if($temp_type == "video"){?>
                                                 <img src="<?php echo image_path('video.png'); ?>" alt="no-img" />
                                                 <?php }else{?>
                                                 <img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
												 <?php }?>
                                            </a>
                                            <h3>
                                            	<a href ="<?php echo url_for('@complaint?id='.$cat->getId());?>">
													<?php $temp_cat = $cat->getCatId(); 
														if($temp_cat < 6){
															echo $cat->getIssueName();
														}else if($temp_cat == 6){
															echo $cat->getVictimName();
														}else if($temp_cat == 7){
															echo $cat->getOverseaName();
														}
													   ?>   
                                                </a>
                                            </h3>
                                            <div class="gllry_cat">
                                                <span><?php echo $cat->getCategory()->getName();?></span>
                                                <span><?php echo $cat->getPlace()->getName();?></span>
                                                <span><?php echo $cat->getIssue()->getName();?></span>
                                            </div>
                                        </li>
                               <?php }
								}?>
                            </div>
                            <div class="clearfix"></div>
                            <?php if($limit_page > $pageLimit){ ?>
                            <button class="show_more">Show more!</button>
                            <div class="loader"></div>
                            <div class="clearfix"></div>
                            <div class="messages">No more results!</div>
                            <?php }else if($complaints->count() == 0){?>
                            <div class="no-rec-messages">No result found!</div>
                            <?php }?>
                         </div>
                        <div class="clearfix"></div>
                        
                    </div>
                </div>
            </div>
    		
   			
     		 
  </div>
</section>
<script typ type="text/javascript">
	$(document).ready(function(){
		$("show_more").show();
		$('.loader').hide();
		$(".messages").hide();
		var page_records = 15;
		var records_to_fetch = 6;
		var page_load = 0;
		var nbr = <?php echo $limit_page;?>;
		var cat = '<?php echo $sf_request->getParameter('cat');?>';
		var pid = '<?php echo $sf_request->getParameter('pid');?>';
		var iid = '<?php echo $sf_request->getParameter('iid');?>';
		var todate = '<?php echo $sf_request->getParameter('to_date');?>';
		var fromdate = '<?php echo $sf_request->getParameter('from_date');?>';
		var province_id = '<?php echo $sf_request->getParameter('filter_province');?>';
		$(".show_more").click(function(){
				$(this).hide();
				$('.loader').show();
				page_load ++;
				if (page_records >= nbr) {
					$(".show_more").hide();
					$('.messages').show();
					$('.loader').hide();
				}
				else{
					var dataString = 'page_load='+page_load+'&cat='+cat+'&pid='+pid+'&iid='+iid+'&to_date='+todate+"&from_date="+fromdate+"&prov_id="+province_id;
					$.ajax({
					  type: "POST",  
					  url: '<?php echo url_for('main/getmorerecords', true); ?>',  
					  data: dataString,
					  //dataType: 'json',
					  success: function(data) {
						$( ".gallery").append(data).fadeIn('slow');
						//$(data).append('.gallery').fadeIn('slow');
						$('.loader').hide();
						$(".show_more").show();
					  }
					  
					});
					page_records += records_to_fetch;
				}
			
				
			
		});
		$("#from_date").datepicker({
			maxDate: "+60D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			  $("#to_date").datepicker("option","minDate", selected)
			}
		});
		$("#to_date").datepicker({ 
			maxDate:"+60D",
			numberOfMonths: 1,
			onSelect: function(selected) {
			   $("#from_date").datepicker("option","maxDate", selected)
			}
		}); 
		$(".reset_btn").click(function(){
			$("#filter_province").val(0);
			$("#cat").val(0);
			$("#to_date").val("To");
			$("#from_date").val("From");
		});

	});
</script>