<div class="section-banner less_padding"> &nbsp; </div>
<section>
  	<div class="section-top">
     		 <div class="container">
    			<div class="row">
                     <div class="top-stion-text">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                          	 <?php include_component('main', 'quickNavigation'); ?>	
                         </div>
                         <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="detail">
                               <div class="detail-img">
                               <?php $temp_img = $user_complaint->getFilePath();
							   $comps_imgs_count = $user_complaint->getIssueImages()->count();
							   $temp_type = $user_complaint->getFileType();
							   if($temp_type == "image"){
									if(isset($temp_img) && $temp_img != ''){
										if($comps_imgs_count == 0){?>
											<img src="<?php echo image_path('/uploads/complaints/'.$user_complaint->getId().'/'.$user_complaint->getFileType().'/'.$temp_img); ?>" alt="<?php echo $user_complaint->getIssueName();?>" />
                                        <?php }else{?>
                                        	<div class="slider_box">
                                            <div id="homepage_slider">
                                        	<ul class="bxslider">
											  <?php foreach($user_complaint->getIssueImages() as $img): ?>
                                                <li>
                                                  <img src="<?php echo image_path('/uploads/complaints/'.$user_complaint->getId().'/'.$img->getFileType().'/'.$img->getFilePath()); ?>" layout="portrait"/>
                                                </li>
                                              <?php endforeach; ?>
                                                
                                              </ul>
                                              </div>
                                              <div class="clear"></div>
                                              </div>
										<?php }?>
									<?php }else{?>
										<img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
									<?php }
							   }else if($temp_type == "audio"){
                               		if(isset($temp_img) && $temp_img != ''){?>
                                    	 <audio controls>
                                              <source src="<?php echo image_path('/uploads/complaints/'.$user_complaint->getId().'/'.$user_complaint->getFileType().'/'.$temp_img); ?>" type="audio/mpeg">
                                         </audio> 
									<?php }else{?>
                                    	<img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
									<?php }?>
							   <?php }else if($temp_type == "video"){
								   if(isset($temp_img) && $temp_img != ''){?>
                                            <video width="320" height="240" controls>
                                                  <source src="<?php echo image_path('/uploads/complaints/'.$user_complaint->getId().'/'.$user_complaint->getFileType().'/'.$temp_img); ?>" type="video/mp4">
                                              </video> 

									<?php }else{?>
                                    	<img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
									<?php }?>
							   <?php }else{?>
		                               <img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
                               <?php }?>
                               </div>
                               <div class="clearfix"></div>
                               <div class="detail-of-img">
                                    <li><label>Tags:</label><span><?php echo $user_complaint->getCategory()->getName()." | ".$user_complaint->getPlace()->getName()." | ".$user_complaint->getIssue()->getName();?></span><div class="clearfix"></div></li>
                                    <?php $temp_cat = $user_complaint->getCatId();
									if($temp_cat < 6){?>
                                    <li><label>Province:</label><span><?php echo $user_complaint->getProvince()->getName();?></span><div class="clearfix"></div></li>
                                    <li><label>City:</label><span><?php echo $user_complaint->getCity()->getName();?></span><div class="clearfix"></div></li>
                                    <li><label>Place:</label><span><?php echo $user_complaint->getIssueName();?></span><div class="clearfix"></div></li>
                                    <?php }else if($temp_cat == 6){?>
                                    <li><label>Victim Name:</label><span><?php echo $user_complaint->getVictimName();?></span><div class="clearfix"></div></li>
                                    <li><label>Victim Age:</label><span><?php echo $user_complaint->getVictimAge();?></span><div class="clearfix"></div></li>
                                    <li><label>Address:</label><span><?php echo $user_complaint->getVictimAddress();?></span><div class="clearfix"></div></li>
                                    <?php }else if($temp_cat == 7){?>
                                    <li><label>Name:</label><span><?php echo $user_complaint->getOverseaName();?></span><div class="clearfix"></div></li>
                                    <li><label>Phone:</label><span><?php echo $user_complaint->getOverseaPhone();?></span><div class="clearfix"></div></li>
                                    <li><label>Consulate:</label><span><?php echo $user_complaint->getOverseaConsulate();?></span><div class="clearfix"></div></li>
									<?php }?>
                               </div> 
                            </div>
                         </div>
                         <div class="clearfix"></div>
                    </div>
                </div>
            </div>
  </div>
</section>
<script language="javascript" type="text/javascript">
  
      $(document).ready(function() {
        //$('div.slider_box img').slidingGallery({ useCaptions: true, container: $('div.slider_box') });

        $('.bxslider').bxSlider({
          mode: 'fade',
          infiniteLoop: true,
          controls: true,
          speed: 800,
          pause: 5000, 
          auto: true,
		  autoControls: false,
          autoDirection: 'next',
          moveSlideQty: 1,
          autoHover: false
        });

      });
</script>