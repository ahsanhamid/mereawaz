<div class="section-banner section_change_margin">
  <div class="container-fluid banner_bg">
    <div class="row">
      <div class="header-banner">
        <ul>
        	<li>
            	<span><img src="<?php echo image_path('ios.png'); ?>" alt="#" /></span>
                <a href="#"><img src="<?php echo image_path('ios_btn.png');?>" alt="#" /></a>
            </li>
            <li>
            	<span><img src="<?php echo image_path('android.png');?>" alt="#"/></span>
                <a href="#"><img src="<?php echo image_path('android_btn.png');?>" alt="#"/></a>
            </li>
        </ul>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>
</div>
<section>
  	<div class="section-top">
            	<div class="container">
                     <div class="row">
                     	 <div class="col-lg-12 col-md-12 col-sm-12">
	                        <h1 class="feature">Categories</h1>
                         </div>
                    </div>
                </div>
                <div class="container">
               		 <div class="row">
             	        <div class="top-stion-text">
                     	<?php if($categories){
							foreach($categories as $cat){?>
	                            <?php $temp_img = $cat->getLogo();?>

                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <div class="img-Features"> 

	                            <a href="<?php echo url_for('@category?cat='.$cat->getId());?>">
 
 
                                <?php if(isset($temp_img) && $temp_img != ''){?>
	                                <img src="<?php echo image_path('/uploads/category/'.$cat->getId().'/'.$temp_img); ?>" alt="<?php echo $cat->getName();?>" />
                                <?php }else{?>
                                    <img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />

								<?php }?>
                                </a>
                            </div>
                            <div class="img-Features-text">
	                            <h4><a href ="#"><?php echo $cat->getName();?></a></h4>
	                            <p><?php echo $cat->getDescription();?></p>
                            </div>    
                        </div>
                        <?php }
						}?>
                	</div>
             	</div>
             </div>
  </div>
</section>