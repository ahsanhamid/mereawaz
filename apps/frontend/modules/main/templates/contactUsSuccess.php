<div class="section-banner less_padding"> &nbsp; </div>
<section>
  	<div class="section-top">
     		 <div class="container">
    			<div class="row">
                   <div class="inner_div">
             	<h1 ><a class="contact-heading" href="#">CONTACT US</a></h1>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div id="wrap">
                        <?php if($sf_user->hasFlash('notice_subs')){?>
	                        <div class="success-msg"><?php echo $sf_user->getFlash('notice_subs');?></div>
                        <?php }?>
                            <form id="contactform" action="<?php echo url_for('@contactus');?>" method="post">
                                <table>
                                    <tr>
                                        <td><?php echo $form['contact_name']->renderLabel(); ?></td>
                                        <td><?php echo $form['contact_name']->render(); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $form['contact_email']->renderLabel(); ?></td>
                                        <td><?php echo $form['contact_email']->render(); ?></td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $form['contact_message']->renderLabel(); ?></td>
                                        <td><?php echo $form['contact_message']->render(); ?></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="submit" value="Send!" id="send" /></td>
                                    </tr>
                                </table>
                            </form>
                            <div id="response"></div>
                        </div>
                    </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col- xs-12">
                	<div class="contact-link">
                    	<ul>
                        	<li class="address">1st Floor 18-D2 Johar Town<br>Khayaban-e-Firdousi, Lahore, Punjab, Pakistan</li>
                            <li class="phone"> 0123456789</li>
                            <li class="web">http://thedevclan.co.uk</li>
                            <li class="mail">abc@thedevclan</li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix"></div>
             </div>  
               </div>
            </div>
    		
   			
     		 
  </div>
</section>