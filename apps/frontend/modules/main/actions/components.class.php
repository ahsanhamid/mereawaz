<?php
class mainComponents extends sfComponents{
	
  public function executeDynamicMenu(sfWebRequest $request) {
	  $categories = Doctrine_Query::create()
                  ->from('Category c')
                  ->orderBy('c.name ASC')
                  ->execute();
    $this->categories = $categories;
	//$action = $this->getContext()->getController();
	//$module = $request->getParameter('action');
	//echo $action."test";exit;
  }
  public function executeQuickNavigation(sfWebRequest $request){
	  $cat = $request->getParameter('cat');
	  $place_id = $request->getParameter('pid');
	  $issue_id = $request->getParameter('iid');
	  $cid =  $request->getParameter('id');
	  $this->param_flag = 0;
	  $this->selected_cat = 0;
	  $this->selected_place = 0;
	  $this->selected_issue = 0;
	  $this->selected_compl = 0;
      if(isset($cat) && $cat != ''){
	  	$this->selected_cat = $cat;
		$this->param_flag = 1;
	  }
	  if(isset($place_id) && $place_id != ''){
		  $catPlaces = Doctrine_Query::create()
				  ->from('CategoryPlace cp')
				  ->where('cp.id = ?',array($place_id))
				  ->fetchOne();
		$this->selected_cat = $catPlaces->getCatId();
	  	$this->selected_place = $catPlaces->getPlaceId();
		$this->param_flag = 1;
	  }
	  if(isset($issue_id) && $issue_id != ''){
		  $placeIssueR = Doctrine_Query::create()
				  ->from('PlaceIssue pi')
				  ->where('pi.id = ?',array($issue_id))
				  ->fetchOne();

	  	$this->selected_issue = $placeIssueR->getIssueId();
		$this->param_flag = 1;
		$this->selected_place = $placeIssueR->getPlaceId();
		$this->selected_issue = $placeIssueR->getIssueId();
		$user_complaint = Doctrine_Query::create()
		  ->from('CategoryPlace cp')
		  ->where('cp.place_id = ?',array($this->selected_place))
		  ->fetchOne();
		$this->selected_cat = $user_complaint->getCatId();
	  }
	  if(isset($cid) && $cid != ''){
	  	$this->selected_compl = $cid;
		$this->param_flag = 1;
		$user_complaint = Doctrine_Query::create()
                  ->from('UserReportedIssue uri')
				  ->where('uri.id = ?',array($cid))
                  ->fetchOne();
		//print_r($user_complaint);exit;
        $this->selected_cat = $user_complaint->getCatId();
		$this->selected_place = $user_complaint->getPlaceId();
		$this->selected_issue = $user_complaint->getIssueId();
		//echo $this->selected_issue;exit;
	  }
	  
	  if($this->param_flag == 0){
	  	$this->selected_cat = 1;
	  }
  	$categories = Doctrine_Query::create()
                  ->from('Category c')
                  ->orderBy('c.id ASC')
                  ->execute();
    $this->categories = $categories;
  }
}?>