<?php

/**
 * main actions.
 *
 * @package    mereawaz
 * @subpackage main
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mainActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
	  $db = Doctrine_Manager::getInstance()->getCurrentConnection();
    /*$categories = Doctrine_Query::create()
                  ->from('Category c')
				  ->orderBy('rand()')
				  ->limit(4)
                  ->execute();
    $this->categories = $categories;*/
	$complaints = Doctrine_Query::create()
                  ->from('UserReportedIssue uri')
				  ->where('uri.is_approved = 1')
				  ->orderBy('uri.created_at desc')
				  ->orderBy('uri.cat_id asc')
				  ->groupBy('uri.cat_id')
                  ->execute();
    $this->complaints = $complaints;
  }
  public function executeAllCategory(sfWebRequest $request)
  {
	  $db = Doctrine_Manager::getInstance()->getCurrentConnection();
    $categories = Doctrine_Query::create()
                  ->from('Category c')
				  ->groupBy('c.id')
                  ->execute();
    $this->categories = $categories;
  }
  
  public function executeCategory(sfWebRequest $request){
	  $this->selected_cat = 0;
	  $this->selected_province = 0;
	  $this->selected_to_date = 0;
	  $this->selected_from_date = 0;
	  $filter_province = Doctrine_Query::create()
                  ->from('Province p')
                  ->execute();
      $this->filter_province = $filter_province;
	  $cat = $request->getParameter('cat');
	  $filter_province = $request->getParameter('filter_province');
	  $from_date = $request->getParameter('from_date');
	  $to_date = $request->getParameter('to_date');
	  $place_id = $request->getParameter('pid');
	  $issue_id = $request->getParameter('iid');
	  $limit_page = 15;
	  $this->pageLimit = $limit_page;
	  $query = Doctrine_Query::create()
			  ->from('UserReportedIssue uri')
			  ->where('uri.is_approved = 1');
	  $param_flag = 0;
      if(isset($cat) && $cat != '' && $cat != 0){
	  	$query->andWhere('uri.cat_id = ?',array($cat));
		$param_flag = 1;
		$this->selected_cat = $cat;
	  }
	  if($cat == 0 || $filter_province == 0){
	  	$param_flag = 1;
	  }
	  if(isset($filter_province) && $filter_province != '' && $filter_province != 0){
	  	$this->selected_province = $filter_province;
		$query->andWhere('uri.province_id = ?',array($filter_province));
		$param_flag = 1;
	  }
	  if(isset($from_date) && $from_date != '' && $from_date != 'From'){
	  	$this->selected_from_date = $from_date;
		$query->andWhere('uri.created_at >= ?',array(date('Y-m-d',strtotime($from_date))));
		$param_flag = 1;
	  }
	  if(isset($to_date) && $to_date != '' && $to_date != 'To'){
	  	$this->selected_to_date = $to_date;
		$query->andWhere('uri.created_at <= ?',array(date('Y-m-d',strtotime($to_date))));
		$param_flag = 1;
	  }
	  if(isset($place_id) && $place_id != ''){
		  $catPlaces = Doctrine_Query::create()
				  ->from('CategoryPlace cp')
				  ->where('cp.id = ?',array($place_id))
				  ->fetchOne();
		$this->selected_cat = $catPlaces->getCatId();
	  	$this->selected_place = $catPlaces->getPlaceId();
	  	$query->andWhere('uri.cat_id = ? and uri.place_id = ?',array($this->selected_cat,$this->selected_place));
		$param_flag = 1;
	  }
	  if(isset($issue_id) && $issue_id != ''){
		  $placeIssueR = Doctrine_Query::create()
				  ->from('PlaceIssue pi')
				  ->where('pi.id = ?',array($issue_id))
				  ->fetchOne();
	  	$query->andWhere('uri.place_id = ?',array($placeIssueR->getPlaceId()));
		$query->andWhere('uri.issue_id = ?',array($placeIssueR->getIssueId()));
		$param_flag = 1;
		$this->selected_cat = $placeIssueR->getPlace()->getPlaceCategories()->get(0)->getCatId();
	  }
	  if($param_flag == 0){
	  	$query->andWhere('uri.cat_id = 1');
		$this->selected_cat = 1;
	  }
	  $complaints_count = $query->orderBy('uri.id DESC')->execute()->count();	  
	  $complaints = $query->orderBy('uri.id DESC')->limit($limit_page)->execute();

  	  $this->limit_page = $complaints_count;
			  //->orderBy('c.id ASC')
			  //->execute();
    $this->complaints = $complaints;
	$categories = Doctrine_Query::create()
                  ->from('Category c')
				  ->orderBy('c.id ASC')
                  ->execute();
    $this->categories = $categories;
  }
  public function executeGetmorerecords(sfWebRequest $request){
  	$this->setLayout(false);
	$records_to_fetch = 6;
	$counter = $request->getParameter('page_load');
	$cat = $request->getParameter('cat');
    $filter_province = $request->getParameter('filter_province');
	$from_date = $request->getParameter('from_date');
	$to_date = $request->getParameter('to_date');
	$place_id = $request->getParameter('pid');
	$issue_id = $request->getParameter('iid');
	$ofset = $counter*$records_to_fetch;
	$query = Doctrine_Query::create()
                  ->from('UserReportedIssue uri')
				  ->where('uri.is_approved = 1');

     $param_flag = 0;
      if(isset($cat) && $cat != '' && $cat != 0){
	  	$query->andWhere('uri.cat_id = ?',array($cat));
		$param_flag = 1;
	  }
	  if(isset($place_id) && $place_id != ''){
	  	$catPlaces = Doctrine_Query::create()
				  ->from('CategoryPlace cp')
				  ->where('cp.id = ?',array($place_id))
				  ->fetchOne();
	  	$query->andWhere('uri.cat_id = ? and uri.place_id = ?',array($catPlaces->getCatId(),$catPlaces->getPlaceId()));
		$param_flag = 1;
	  }
	  if(isset($issue_id) && $issue_id != ''){
		$placeIssueR = Doctrine_Query::create()
				  ->from('PlaceIssue pi')
				  ->where('pi.id = ?',array($issue_id))
				  ->fetchOne();
	  	$query->andWhere('uri.place_id = ?',array($placeIssueR->getPlaceId()));
		$query->andWhere('uri.issue_id = ?',array($placeIssueR->getIssueId()));
		$param_flag = 1;
	  }
	  if(isset($filter_province) && $filter_province != '' && $filter_province != 0){
	  	$this->selected_province = $filter_province;
		$query->andWhere('uri.province_id = ?',array($filter_province));
		$param_flag = 1;
	  }
	  if(isset($from_date) && $from_date != '' && $from_date != 'From'){
	  	$this->selected_from_date = $from_date;
		$query->andWhere('uri.created_at >= ?',array(date('Y-m-d',strtotime($from_date))));
		$param_flag = 1;
	  }
	  if(isset($to_date) && $to_date != '' && $to_date != 'To'){
	  	$this->selected_to_date = $to_date;
		$query->andWhere('uri.created_at <= ?',array(date('Y-m-d',strtotime($to_date))));
		$param_flag = 1;
	  }
	  if($cat == 0 || $filter_province == 0){
	  	$param_flag = 1;
	  }
	  if($param_flag == 0){
	  	$query->andWhere('uri.cat_id = 1');
	  }
	  
	  $user_complaints = $query->orderBy('uri.id DESC')->limit($records_to_fetch)->offset($ofset)->execute();
	  //echo $query->getSqlQuery();
	  //print_r($user_complaints);exit;
	  $return_html = '';
	  foreach($user_complaints as $uc){
		$temp_file = $uc->getFilePath();
		$temp_type = $uc->getFileType();
		$temp_url = '';
		if(isset($temp_file) && $temp_file != ''){
			if($temp_type == 'image'){
				$temp_url = '/uploads/complaints/'.$uc->getId().'/'.$temp_type.'/'.$temp_file;
			}else if($temp_type == 'audio'){
				$temp_url = '/images/audio.png';
			}else if($temp_type == 'video'){
				$temp_url = '/images/video.png';
			}
		}else{
			$temp_url = '/images/no-img.png';
		}
	  	$return_html .= '<li><a rel="example_group" href="'.$this->generateUrl("complaint", array('id'=>$uc->getId())).'" title="'.$uc->getIssueName().'"><img src="'.$temp_url.'" alt="no-img" /></a> <h3><a href ="'.$this->generateUrl("complaint", array('id'=>$uc->getId())).'">'.$uc->getIssueName().'</a></h3><div class="gllry_cat"><span>'.$uc->getCategory()->getName().'</span><span>'.$uc->getPlace()->getName().'</span><span>'.$uc->getIssue()->getName().'</span></div></li>';
	  }
	  echo $return_html;
	  return sfView::NONE;
  }
  public function executeComplaint(sfWebRequest $request){
    $cid = $request->getParameter('id');
	$user_complaint = Doctrine_Query::create()
                  ->from('UserReportedIssue uri')
				  ->where('uri.id = ?',array($cid))
                  ->fetchOne();
	//print_r($user_complaint);exit;
    $this->user_complaint = $user_complaint;
  }
  public function executeContactUs(sfWebRequest $request)
  {
    
    $this->form = new sfForm();
    $this->form->setWidgets(array(
        'contact_name' => new sfWidgetFormInputText(array('label' => 'Name'),array('placeholder' => 'Name')),
        'contact_email' => new sfWidgetFormInputText(array('label' => 'Email'),array('placeholder' => 'Email')),
        'contact_message' => new sfWidgetFormTextarea(array('label' => 'Message'),array('placeholder' => 'Message')),
    ));
    $this->form->setValidators(array(
        'contact_name' => new sfValidatorString(array('min_length' => 4),array(
                              'required'   => 'Name is required')),
        'contact_email' => new sfValidatorEmail(array(),array(
                              'required'   => 'Email is required',
                              'invalid'    => 'You must enter a valid email address')),
        'contact_message' => new sfValidatorString(array('min_length' => 4),array(
                              'required'   => 'Message is required','min_length'=>'message too short'))
    ));
    
    if ($request->isMethod('post'))
    {
      $contact_name = $request->getParameter('contact_name');
      $contact_email = $request->getParameter('contact_email');
      $contact_message = $request->getParameter('contact_message');
      $this->form->bind(array('contact_name'=>$contact_name,
                              'contact_email'=>$contact_email,
                              'contact_message'=>$contact_message));
      if ($this->form->isValid()) 
      {
        
        $email_to = sfConfig::get('app_contact_email_to');

        $mailer = $this->getMailer();
        $mailer->getTransport()->setLocalDomain('[127.0.0.1]');
        $message = $mailer->compose();
        $message->setSubject('meriawaz Contact');
        $message->setTo($email_to);
        $message->setFrom($email_to, 'meriawaz Contact');
        
        $contact_message = "Name: ".$contact_name.'<br/>'."Email: ".$contact_email.'<br/>'."Message: <br/>".$contact_message;
        $message->setBody(nl2br($contact_message), 'text/html');
        $mailer->send($message);
        
        $this->getUser()->setFlash('notice_subs', "Your message has been sent successfully");
        $this->redirect('@contactus');
      }
    }
  
  }
  public function executeAboutUs(sfWebRequest $request)
  {
  }
}
