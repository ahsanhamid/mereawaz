<?php

/**
 * api actions.
 *
 * @package    mereawaz
 * @subpackage api
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class apiActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
  public function executeGetCategory(sfWebRequest $request){
  	$db = Doctrine_Manager::getInstance()->getCurrentConnection();
	$resultArray = array();
	$error = 0;
	$query = "select c.id as category_id, c.name as category_name, c.description as category_desc, c.logo as category_logo, c.urdu_name as cat_urdu_name, 'category_place' from category c";
	$pdo = $db->execute($query);
	$pdo->setFetchMode(Doctrine_Core::FETCH_ASSOC);
	$category = $pdo->fetchAll();
	$category_counter = 0;
	foreach($category as $cat){
		$query = "select p.id as place_id, p.name as place_name, p.description as place_desc, p.logo as place_logo, p.urdu_name as place_urdu_name, 'place_issue' from place p left join category_place cp on p.id = cp.place_id where cp.cat_id = ".$cat['category_id'];
		$pdo = $db->execute($query);
		$pdo->setFetchMode(Doctrine_Core::FETCH_ASSOC);
		$places = $pdo->fetchAll();
		$main_counter = 0;
		foreach($places as $place){
			$query = "select i.id as issue_id, i.name as issue_name, i.description as issue_desc, i.logo as issue_logo, i.urdu_name as issue_urdu_name from issue i left join place_issue pi on i.id = pi.issue_id where pi.place_id = ".$place['place_id'];
			$pdo = $db->execute($query);
			$pdo->setFetchMode(Doctrine_Core::FETCH_ASSOC);
			$places[$main_counter]['place_issue'] = $pdo->fetchAll();
			$main_counter++;
		}
		$category[$category_counter]['category_place'] = $places;
		$category_counter++;
	}
	if($category){
		$resultArray['status'] = 'success';
		$resultArray['category'] = $category;
		$resultArray['response'] = 1;
	}else{
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = "No results found";
		$resultArray['response'] = 0;
		$error = 1;
	}
	$this->getResponse()->setContentType('application/json');
    return $this->renderText(json_encode($resultArray));  
  }
  public function executeGetProvince(sfWebRequest $request){
  	$db = Doctrine_Manager::getInstance()->getCurrentConnection();
	$resultArray = array();
	$error = 0;
	$query = "select p.id as province_id, p.name as province_name, p.urdu_name as province_urdu_name from province p";
	$pdo = $db->execute($query);
	$pdo->setFetchMode(Doctrine_Core::FETCH_ASSOC);
	$province = $pdo->fetchAll();
		if($province){
		$resultArray['status'] = 'success';
		$resultArray['category'] = $province;
		$resultArray['response'] = 1;
	}else{
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = "No results found";
		$resultArray['response'] = 0;
		$error = 1;
	}
	$this->getResponse()->setContentType('application/json');
    return $this->renderText(json_encode($resultArray));  
  }
  public function executeUserSignup(sfWebRequest $request){
  	$db = Doctrine_Manager::getInstance()->getCurrentConnection();
	$resultArray = array();
	$error = 0;
	$email = $request->getParameter('user_email');
	$password = $request->getParameter('user_password');
	$lat = $request->getParameter('lat');
	$long = $request->getParameter('long');
	if(!isset($email) || $email == ""){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = "Email address missing";
		$resultArray['response'] = 2;
		$error = 1;
	}
	if(!isset($password) || $password == ""){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = "password missing";
		$resultArray['response'] = 3;
		$error = 1;
	}
	$sfGuardUserTemp = Doctrine_Core::getTable('sfGuardUser')->findOneByEmailAddress($email);
	if($sfGuardUserTemp){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = 'Email address already in use';
		$resultArray['errorCode'] = 8;
		$error = 1;
	}
	if($error == 0){
		$conn = Doctrine_Manager::getInstance()->getCurrentConnection();
		$conn->beginTransaction();
		try{
			$user = new sfGuardUser();
			$user->setUsername($email);
			$user->setPassword($password);
			$user->setEmailAddress($email);
			$user->save();
		}catch(Exception $e){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Unable to create app user';
			$resultArray['errorCode'] = 4;
			$error = 1;
		}
		try{
			$appUser = new ApplicationUser();
			$appUser->setUserId($user->getId());
			$appUser->setLongitude($long);
			$appUser->setLatitude($lat);
			$appUser->save();
		}catch(Exception $e){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Unable to create app user';
			$resultArray['errorCode'] = 5;
			$error = 1;
		}
		$conn->commit();
	}
	if($error == 0){
		$query = "select au.id as user_id, au.name as display_name, au.address, au.cnic, au.phone, au.language, au.is_verified, au.latitude, au.longitude, pu.id as sf_id,pu.email_address from application_user au left join sf_guard_user pu on au.user_id = pu.id where pu.email_address = '".$email."' group by pu.id";
		$pdo = $db->execute($query);
		$pdo->setFetchMode(Doctrine_Core::FETCH_ASSOC);
		$cuser = $pdo->fetchAll();
		$resultArray['status'] = 'success';
		$resultArray['user'] = $cuser;
		$resultArray['response'] = 1;
	}
	$this->getResponse()->setContentType('application/json');
    return $this->renderText(json_encode($resultArray));  
  }
  public function executeUserLogin(sfWebRequest $request){
  	$db = Doctrine_Manager::getInstance()->getCurrentConnection();
	$resultArray = array();
	$error = 0;
	$email = $request->getParameter('user_email');
	$password = $request->getParameter('user_password');
		if(!isset($email) || $email == ""){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = "Email address missing";
		$resultArray['response'] = 2;
		$error = 1;
	}
	if(!isset($password) || $password == ""){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = "password missing";
		$resultArray['response'] = 3;
		$error = 1;
	}
	if($error == 0){
		$sfGuardUser = Doctrine_Core::getTable('sfGuardUser')->findOneByEmailAddress($email);
		if($sfGuardUser){
			$checkPass = $sfGuardUser->checkPassword($password);
			if($checkPass){
				$query = "select au.id as user_id, au.name as display_name, au.address, au.cnic, au.phone, au.language, au.is_verified, au.latitude, au.longitude, pu.id as sf_id,pu.email_address from application_user au left join sf_guard_user pu on au.user_id = pu.id where pu.email_address = '".$email."' group by pu.id";
				$pdo = $db->execute($query);
				$pdo->setFetchMode(Doctrine_Core::FETCH_ASSOC);
				$cuser = $pdo->fetchAll();
				$resultArray['status'] = 'success';
				$resultArray['user'] = $cuser;
				$resultArray['response'] = 1;
			}else{
				$resultArray['status'] = 'fail';
				$resultArray['msg'] = "Wrong password";
				$resultArray['response'] = 5;
				$error = 1;				
			}
		}else{
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = "Email not found";
			$resultArray['response'] = 4;
			$error = 1;
		}
	}
	$this->getResponse()->setContentType('application/json');
    return $this->renderText(json_encode($resultArray));  
  }
  public function executeUpdateSettings(sfWebRequest $request){
  	$db = Doctrine_Manager::getInstance()->getCurrentConnection();
	$resultArray = array();
	$error = 0;
	$uid = $request->getParameter('user_id');
	$dp_name = $request->getParameter('user_display_name');
	$cnic = $request->getParameter('cnic');
	$address = $request->getParameter('user_address');
	$phone = $request->getParameter('user_phone_number');
	$lang = $request->getParameter('user_selected_language');
	if(!isset($uid) || $uid == ''){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = 'User ID missing';
		$resultArray['errorCode'] = 0;
		$error = 1;
	}else{
		$user = Doctrine_Query::create()
				->from('ApplicationUser au')
				->where('au.id = ?',array($uid))
				->fetchOne();
		if($user){
			if(isset($dp_name) && $dp_name != ''){
				$user->setName($dp_name);
			}
			if(isset($address) && $address != ''){
				$user->setAddress($address);
			}
			if(isset($phone) && $phone != ''){
				$user->setPhone($phone);
			}
			if(isset($lang) && $lang != ''){
				$user->setLanguage($lang);
			}
			if(isset($cnic) && $cnic != ''){
				$user->setCnic($cnic);
			}
			$user->save();
		}else{
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Invalid user ID';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
	}
	if($error == 0){
		$resultArray['status'] = 'success';
		$resultArray['msg'] = 'User updated successfully';
		$resultArray['errorCode'] = 1;
		$query = "select au.id as user_id, au.name as display_name, au.address, au.cnic, au.phone, au.language, au.is_verified, au.latitude, au.longitude, pu.id as sf_id,pu.email_address from application_user au left join sf_guard_user pu on au.user_id = pu.id where au.id = ".$uid." group by pu.id";
		$pdo = $db->execute($query);
		$pdo->setFetchMode(Doctrine_Core::FETCH_ASSOC);
		$cuser = $pdo->fetchAll();
		$resultArray['user'] = $cuser;
		$resultArray['errorCode'] = 1;
	}
	$this->getResponse()->setContentType('application/json');
    return $this->renderText(json_encode($resultArray));  
  }
  public function executeForgotPassword(sfWebRequest $request){
  	$email = $request->getParameter('user_email');
   $tempPass = $this->genPassword(6);
   $error = 0;
   $resultArray= array();
   if($email == "" || !isset($email)){
		$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Email missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
	}else{
		$sfGuardUser = Doctrine_Core::getTable('sfGuardUser')->findOneByEmailAddress($email);
		if($sfGuardUser){
			$sfGuardUser->setPassword($tempPass);
			$sfGuardUser->save();
			$resultArray['status'] = 'success';
			$resultArray['errorCode'] = 1;
			$resultArray['msg'] = 'Password reset and emailed';
			$email_to = $email;
			$name = $sfGuardUser->getAppUser()->getName();
			$email_from = 'support@mereawaz.com';
			$email_body = "Hi [user],<br/> Your new password is [password]<br /><br />Thank you,<br />Mereawaz";
			$email_body = str_replace('[user]', $name, $email_body);
			$email_body = str_replace('[password]', $tempPass, $email_body);
			$email_sub = 'Mereawaz: Password Reminder';
	
			$mailer = $this->getMailer();
			//$mailer->getTransport()->setLocalDomain('[127.0.0.1]');
			$message = $mailer->compose();
			$message->setSubject($email_sub);
			$message->setTo($email_to);
			$email_from_name = "Mereawaz";
			$message->setFrom($email_from, $email_from_name);
	
			$message->setBody($email_body, 'text/html');
			$mailer->send($message);

		}else{
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Email not found';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
	}
	$this->getResponse()->setContentType('application/json');
	return $this->renderText(json_encode($resultArray));  
  }
  public function executeRegisterComplaint(sfWebRequest $request){
	  //print_r($_FILES);exit;
	  $db = Doctrine_Manager::getInstance()->getCurrentConnection();
	$resultArray = array();
	$error = 0;
	$uid = $request->getParameter('user_id');
	$cat_id = $request->getParameter('cat_id');
	$place_id = $request->getParameter('place_id');
	$issue_id = $request->getParameter('issue_id');
	$city = $request->getParameter('city');
	$province_id = $request->getParameter('province_id');
	$place_text = $request->getParameter('place_text');
	$file_type = $request->getParameter('file_type');
	$files = $request->getFiles('file_content');
	$message = $request->getParameter('message');
	$vname = $request->getParameter('victim_name');
	$vaddress = $request->getParameter('victim_address');
	$vage = $request->getParameter('victim_age');
	$oversea_name = $request->getParameter('oversea_name');
	$oversea_phone = $request->getParameter('oversea_phone');
	$oversea_consulate = $request->getParameter('oversea_consulate');
	$comp_lat = $request->getParameter('lat');
	$comp_long = $request->getParameter('long');
	$token = $request->getParameter('user_token');
	if(!isset($uid) || $uid == ''){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = 'User ID missing';
		$resultArray['errorCode'] = 0;
		$error = 1;
	}else if($uid == 0){ // in case of anonymous user
		if(!isset($token) || $token == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'User token missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}else{
			$token_found = Doctrine_Query::create()
					->from('ApplicationUser au')
					->where('au.token like "'.$token.'"')
					->fetchOne();
			if($token_found){
				$uid = $token_found->getId();
			}else{
				$conn = Doctrine_Manager::getInstance()->getCurrentConnection();
				$conn->beginTransaction();
				try{
					$user = new sfGuardUser();
					$user->setUsername($token);
					$user->setPassword("123mereawaz456");
					$user->setEmailAddress($token);
					$user->save();
				}catch(Exception $e){
					$resultArray['status'] = 'fail';
					$resultArray['msg'] = 'Unable to process user token';
					$resultArray['errorCode'] = 14;
					$error = 1;
				}
				try{
					$appUser = new ApplicationUser();
					$appUser->setUserId($user->getId());
					$appUser->setToken($token);
					$appUser->save();
					$uid = $appUser->getId();
				}catch(Exception $e){
					$resultArray['status'] = 'fail';
					$resultArray['msg'] = 'Unable to process user token id';
					$resultArray['errorCode'] = 15;
					$error = 1;
				}
				$conn->commit();
			}
		}
	}
	if(!isset($cat_id) || $cat_id == ''){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = 'Category ID missing';
		$resultArray['errorCode'] = 0;
		$error = 1;
	}
	if(!isset($place_id) || $place_id == ''){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = 'Place ID missing';
		$resultArray['errorCode'] = 0;
		$error = 1;
	}
	if(!isset($issue_id) || $issue_id == ''){
		$resultArray['status'] = 'fail';
		$resultArray['msg'] = 'Issue ID missing';
		$resultArray['errorCode'] = 0;
		$error = 1;
	}
	if($cat_id >= 1 && $cat_id < 6){
		if(!isset($city) || $city == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'City name missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
		if(!isset($province_id) || $province_id == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Province id missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
		if(!isset($place_text) || $place_text == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Place/location text missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
	}else if($cat_id == 6){
		if(!isset($vname) || $vname == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Victim name missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
		if(!isset($vaddress) || $vaddress == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Victim address missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
		if(!isset($vage) || $vage == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Victim age missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
	}else if($cat_id == 7){
		if(!isset($oversea_name) || $oversea_name == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Oversea name missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
		if(!isset($oversea_phone) || $oversea_phone == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Oversea phone missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
		if(!isset($oversea_consulate) || $oversea_consulate == ''){
			$resultArray['status'] = 'fail';
			$resultArray['msg'] = 'Oversea consulate missing';
			$resultArray['errorCode'] = 0;
			$error = 1;
		}
	}
	$temp_comp_id = '';
	if($error == 0){
		$uc = new UserReportedIssue();
		$unique_flag = true;
		while($unique_flag){
			$temp_comp_id = $this->genPassword(5);
			$query_found = Doctrine_Query::create()
					->from('UserReportedIssue uri')
					->where('uri.user_inquiry_unique_name like "'.$uid.'"')
					->fetchOne();
			if($query_found){
				$unique_flag = true;
			}else{
				$unique_flag = false;
			}
		}
		$uc->setUserId($uid);
		$uc->setUserInquiryUniqueName($temp_comp_id);
		$uc->setCatId($cat_id);
		$uc->setPlaceId($place_id);
		$uc->setIssueId($issue_id);
		$uc->setCityID(1); /// because its redundant field so setting it static value
		if($cat_id >= 1 && $cat_id < 6){
			$uc->setCityName($city);
			$uc->setProvinceId($province_id);
			$uc->setIssueName($place_text);
		}else if($cat_id == 6){
			$uc->setVictimName($vname);
			$uc->setVictimAddress($vaddress);
			$uc->setVictimAge($vage);
		}else if($cat_id == 7){
			$uc->setOverseaName($oversea_name);
			$uc->setOverseaPhone($oversea_phone);
			$uc->setOverseaConsulate($oversea_consulate);
		}
		$uc->setLongitude($comp_long);
		$uc->setLatitude($comp_lat);
		$uc->setIssueDesc($message);
		$uc->setFileType($file_type);
		$uc->save();//print_r($file);die('wwe');
		if($file_type == "image" ){
			$img_counter = 0;
			foreach($files as $file){
				if ($file['size'] != 0) {
				  $upload_dir = sfConfig::get('sf_upload_dir');
				  $upload_dir_path = $upload_dir . '/complaints/'.$uc->getId();
				  if (!file_exists($upload_dir_path)) {
					if(mkdir($upload_dir_path)){
						$resultArray['status_msg'] = 'directory created';
					}else{
						$resultArray['status_msg'] = 'directory missed';
						$resultArray['path'] = $upload_dir_path;
						$error = 1;
					}
				  }
				  $upload_dir_path .= '/'.$file_type;
				  if (!file_exists($upload_dir_path)) {
					if(mkdir($upload_dir_path)){
						$resultArray['status_msg'] = 'directory created';
					}else{
						$resultArray['status_msg2'] = 'directory missed 2';
						$resultArray['path2'] = $upload_dir_path;
						$error = 1;
					}
				  }
				  $image_name = strtolower(str_replace(' ', '_', $file['name']));
				  $file_name = $uc->getId().'_'.$image_name;
				  $file_path = $upload_dir_path.'/'.$file_name;
				  if(move_uploaded_file($file['tmp_name'], $file_path)){
					$resultArray['status'] = 'file moved';
					$resultArray['msg'] = 'success';
				  }else{
					$resultArray['status'] = 'file not moved';
					$error = 1;
				  }
				  if($img_counter == 0){
					  $uc->setFilePath($file_name);
					  $uc->save();
					  $issue_images = new IssueImage();
					  $issue_images->setFileType($file_type);
					  $issue_images->setFilePath($file_name);
					  $issue_images->setIssueId($uc->getId());
					  $issue_images->save();
				  }else{
	                  $issue_images = new IssueImage();
					  $issue_images->setFileType($file_type);
					  $issue_images->setFilePath($file_name);
					  $issue_images->setIssueId($uc->getId());
					  $issue_images->save();
				  }
				}
				$img_counter++;
			}
		}else if($file_type == "audio" || $file_type == "video"){
			foreach($files as $file){
				if ($file['size'] != 0) {
			  
				  $upload_dir = sfConfig::get('sf_upload_dir');
				  $upload_dir_path = $upload_dir . '/complaints/'.$uc->getId();
				  if (!file_exists($upload_dir_path)) {
					if(mkdir($upload_dir_path)){
						$resultArray['status_msg'] = 'directory created';
					}else{
						$resultArray['status_msg'] = 'directory missed';
						$resultArray['path'] = $upload_dir_path;
						$error = 1;
					}
				  }
				  $upload_dir_path .= '/'.$file_type;
				  if (!file_exists($upload_dir_path)) {
					if(mkdir($upload_dir_path)){
						$resultArray['status_msg'] = 'directory created';
					}else{
						$resultArray['status_msg2'] = 'directory missed 2';
						$resultArray['path2'] = $upload_dir_path;
						$error = 1;
					}
				  }
				  $image_name = strtolower(str_replace(' ', '_', $file['name']));
				  $file_name = $uc->getId().'_'.$image_name;
				  $file_path = $upload_dir_path.'/'.$file_name;
				  if(move_uploaded_file($file['tmp_name'], $file_path)){
					$resultArray['status'] = 'file moved';
					$resultArray['msg'] = 'success';
				  }else{
					$resultArray['status'] = 'file not moved';
					$error = 1;
				  }
				   
				  $uc->setFilePath($file_name);
				  $uc->save();
				}
			}
		}
	}
	if($error == 0){
		$resultArray['status'] = 'success';
		$resultArray['msg'] = 'Your complaint has been successfully registered';
		$resultArray['complaintID'] = $temp_comp_id;
		$resultArray['errorCode'] = 1;
	}
	$this->getResponse()->setContentType('application/json');
    return $this->renderText(json_encode($resultArray));  
  }
  public function executeGetUserComplaints(sfWebRequest $request){
  	$uid = $request->getParameter('user_id');
	$db = Doctrine_Manager::getInstance()->getCurrentConnection();
	$resultArray = array();
	$error = 0;
	$query = "select uri.* from user_reported_issue uri where uri.user_id = ".$uid;
	$pdo = $db->execute($query);
	$pdo->setFetchMode(Doctrine_Core::FETCH_ASSOC);
	$userComplaints = $pdo->fetchAll();
	if($userComplaints){
		$resultArray['status'] = 'success';
		$resultArray['complaints'] = $userComplaints;
		$resultArray['errorCode'] = 1;		
	}else{
		$resultArray['status'] = 'success';
		$resultArray['msg'] = 'No complaint registered by this user';
		$resultArray['errorCode'] = 1;
	}
	$this->getResponse()->setContentType('application/json');
    return $this->renderText(json_encode($resultArray)); 
  }
  private function genPassword($length) {
    $password = '';
    $p_chars = "9876543210abcdefghijklmnopqrstuvwxyz";
    while (strlen($password) < $length) {
      $password .= $p_chars{rand() % (strlen($p_chars))};
    }
    return $password;
  }
}
