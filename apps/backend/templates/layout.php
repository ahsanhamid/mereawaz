<?php
$action = $sf_context->getActionName();
$module = $sf_context->getModuleName();
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <?php include_http_metas() ?>
    <?php include_metas() ?>
    <?php include_title() ?>
    <link rel="shortcut icon" href="<?php echo image_path('favicon01.png');?>" />
    <?php include_stylesheets() ?>
    <?php include_javascripts() ?>
    <script type="text/javascript">
      var sf_web_dir = '<?php echo $sf_request->getRelativeUrlRoot(); ?>';
	  var base_url = '<?php echo $sf_request->getUriPrefix().$sf_request->getRelativeUrlRoot();?>';
    </script>
  </head>
  <body>
  
    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">
          <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>
          <a class="brand" href="<?php echo url_for('@homepage'); ?>">Mereawaz</a>
          <div class="nav-collapse collapse">
            <p class="navbar-text pull-right">
<!--              Logged in as <a href="#" class="navbar-link"><?php //echo $sf_user; ?></a>&nbsp;-->
              <a href="<?php echo url_for("@sf_guard_signout") ?>"><?php echo 'Logout'; ?></a>
            </p>
            <ul class="nav">
              <li class="<?php echo (($module == "main")?'active':'');?>"><a href="<?php echo url_for('@homepage'); ?>">Home</a></li>
              <li class="<?php echo (($module == "app_users")?'active':'');?>"><a href="<?php echo url_for('@users');?>">Users</a></li>
              <li class="<?php echo (($module == "complaints")?'active':'');?>"><a href="<?php echo url_for('complaints/index');?>">Complaints</a></li>
           </ul>
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>
     <div class="container-fluid">
      <div class="row-fluid">
  		
    <?php echo $sf_content ?>
    	
        </div>
    </div>
    <hr/>
    <div style="clear:both;"></div>
     <footer>
        <p style="padding-top:15px;">&copy; Mereawaz <?php echo date('Y'); ?></p>
      </footer>
  </body>
</html>
