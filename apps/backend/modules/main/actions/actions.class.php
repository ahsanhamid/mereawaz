<?php

/**
 * main actions.
 *
 * @package    mereawaz
 * @subpackage main
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mainActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function preExecute()
  {
   sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
   parent::preExecute();
  }
  public function executeIndex(sfWebRequest $request)
  {
    //$this->forward('default', 'module');
  }
}
