<?php

/**
 * complaints actions.
 *
 * @package    mereawaz
 * @subpackage complaints
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class complaintsActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function preExecute()
  {
   sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
   parent::preExecute();
  }
  public function executeIndex(sfWebRequest $request)
  {
    $complaints = Doctrine_Query::create()
                    ->from('UserReportedIssue uri')
					->orderBy('uri.id desc')
                    ->execute();
    
    $this->complaints = $complaints;
  }
  public function executeNew(sfWebRequest $request){
	  
  }
  public function executeEdit(sfWebRequest $request){
	  
	$id = $request->getParameter('id');
    $complaint = Doctrine_Query::create()
                       ->from('UserReportedIssue uri')
                       ->where('uri.id = ?',array($id))
                       ->fetchOne();
     if(!$complaint) {
      $this->redirect('@homepage');
    }
    $this->complaint = $complaint;
    $default_array = array();
    $default_array['id'] =  $id;
    $default_array['cat_id'] =  $complaint->getCatId();
    $default_array['place_id'] =  $complaint->getPlaceId();
	$default_array['issue_id'] =  $complaint->getIssueId();
    $default_array['city_name'] =  $complaint->getCityName();
	$default_array['province_id'] =  $complaint->getProvinceId();
    $default_array['issue_name'] =  $complaint->getIssueName();
	$default_array['issue_desc'] =  $complaint->getIssueDesc();
    $default_array['file_type'] =  $complaint->getFileType();
	$default_array['victim_name'] =  $complaint->getVictimName();
	$default_array['victim_address'] =  $complaint->getVictimAddress();
	$default_array['victim_age'] =  $complaint->getVictimAge();
    $default_array['oversea_name'] =  $complaint->getOverseaName();
	$default_array['oversea_phone'] =  $complaint->getOverseaPhone();
	$default_array['oversea_consulate'] =  $complaint->getOverseaConsulate();
    $tempStatus = $complaint->getIsApproved();
	if($tempStatus){
		$default_array['is_approved'] =  1;
	}else{
		$default_array['is_approved'] =  0;
	}
    $default_array['user_inquiry_unique_name'] =  $complaint->getUserInquiryUniqueName();
	//print_r($default_array);exit;
	$file_type_array = array("image"=>"image","audio"=>"audio","video"=>"video");
	$category_query = Doctrine_Query::create()
                       ->from('Category')
					   ->orderBy('id')
                       ->execute();
	$cat_array = array();
	foreach($category_query as $cq){
		$cat_array[$cq->getId()] = $cq->getName();
	}
	$place_query = Doctrine_Query::create()
                       ->from('Place p,p.PlaceCategories pc')
					   ->where('pc.cat_id = ?',array($category_query->get(0)->getId()))
					   ->orderBy('p.id')
                       ->execute();
	$place_array = array();
	foreach($place_query as $cq){
		$place_array[$cq->getId()] = $cq->getName();
	}
	$issue_query = Doctrine_Query::create()
                       ->from('Issue i,i.IssuePlaces ip')
					   ->where('ip.place_id = ?',array($place_query->get(0)->getId()))
                       ->execute();
	$issue_array = array();
	foreach($issue_query as $cq){
		$issue_array[$cq->getId()] = $cq->getName();
	}
	$province_query = Doctrine_Query::create()
                       ->from('Province')
					   ->orderBy('id')
                       ->execute();
	$province_array = array();
	foreach($province_query as $cq){
		$province_array[$cq->getId()] = $cq->getName();
	}
    $this->form = new sfForm($default_array);
	$this->file_type = $complaint->getFileType();
	$this->file_content = $complaint->getFilePath();
    $this->form->setWidgets(array(
        'user_inquiry_unique_name' => new sfWidgetFormInputText(array('label' => 'Complaint Name'),array('readonly'=>true)),
        'issue_desc'  => new sfWidgetFormTextarea(),
		'file_type' => new sfWidgetFormChoice(array('expanded'=>false,'multiple' => false,'choices'=>$file_type_array)),
		'file_path' => new sfWidgetFormInputFile(),
        'is_approved' => new sfWidgetFormChoice(array('choices'=>array('1'=>'Yes','0'=>'No'))),
        ));
    
    $this->form->setValidators(array(
        'user_inquiry_unique_name' => new sfValidatorString(array('required'=>false),array('required'=>__('Required'))),
		'issue_desc' => new sfValidatorString(array('required'=>false)),
        'file_type' => new sfValidatorChoice(array('multiple' => false,'choices'=>  array_keys($file_type_array),'required'=>true),array('required' => __('required'))),
		'file_path' => new sfValidatorFile(array(
					  'mime_type_guessers'=>array(),'required'=>false,)),
        'is_approved' => new sfValidatorChoice(array('required'=>true,'choices'=>array(1,0)),array('required'=>__('Required'))),

      ));
    if($request->getMethod() == 'POST')
    { 
      $this->form->bind($request->getPostParameters(),$request->getFiles());
      if ($this->form->isValid()) 
      {
        $issue_desc = $request->getParameter('issue_desc');
		$file_type = $request->getParameter('file_type');
		$file = $request->getFiles('file_path');
		$is_approved = $request->getParameter('is_approved');
        
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();
        

        $complaint->setIssueDesc($issue_desc);
		
		$complaint->setFileType($file_type);
		$complaint->setIsApproved($is_approved);
		if($file_type == "image" || $file_type == "audio" || $file_type == "video"){
			if ($file['size'] != 0) {
          
			  $upload_dir = sfConfig::get('sf_upload_dir');
			  $upload_dir_path = $upload_dir . '/complaints/'.$complaint->getId();
			  if (!file_exists($upload_dir_path)) {
				if(mkdir($upload_dir_path)){
					$resultArray['status_msg'] = 'directory created';
				}else{
					$resultArray['status_msg'] = 'directory missed';
					$resultArray['path'] = $upload_dir_path;
				}
			  }
			  $upload_dir_path .= '/'.$file_type;
			  if (!file_exists($upload_dir_path)) {
				if(mkdir($upload_dir_path)){
					$resultArray['status_msg'] = 'directory created';
				}else{
					$resultArray['status_msg2'] = 'directory missed 2';
					$resultArray['path2'] = $upload_dir_path;
				}
			  }
			  $image_name = strtolower(str_replace(' ', '_', $file['name']));
			  $file_name = $complaint->getId().'_'.$image_name;
			  $file_path = $upload_dir_path.'/'.$file_name;
			  if(move_uploaded_file($file['tmp_name'], $file_path)){
			  	$resultArray['status'] = 'file moved';
				$resultArray['msg'] = 'success';
			  }else{
			  	$resultArray['status'] = 'file not moved';
			  }
			   
			  $complaint->setFilePath($file_name);
			  $complaint->save();
			}
		}
		$complaint->save();
		
		$conn->commit();
		
        $this->getUser()->setFlash('notice', 'Complaint information updated successfully');
        $this->redirect('complaints/edit?id='.$id);
      }
    }
  
  }
  public function executeDelete(sfWebRequest $request){
  }
}
