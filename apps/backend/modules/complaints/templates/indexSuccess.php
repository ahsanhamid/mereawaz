<?php use_helper('I18N', 'Date') ?>
<style type="text/css">
#sf_admin_container #sf_admin_bar{
	display:block !important;
}
</style>
<div id="push"></div>
<div class="page-header">
  
  
  
  <h1>Complaints list</h1>
  
  
</div>


<div id="sf_admin_container">
<?php if($sf_user->hasFlash('notice')): ?>
  <div class="notice"><?php echo $sf_user->getFlash('notice'); ?></div>
  <?php endif; ?>
</div>





<div class="sf_admin_list">
<table class="table table-bordered table-striped tablesorter stretch" id="sort">
    <thead>
      <tr>
        <th>Place</th>    
        <th>Category</th>
        <th>Sub Category 1</th>
        <th>Sub Category 2</th>
        <th>Is Approved</th>
        <th>Created At</th>
        <th>Update At</th>    
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        foreach($complaints as $e): 
        
      ?>
      <tr id="item-<?php echo $e->getId();?>">
        <td><?php echo $e->getIssueName(); ?></td>
        <td><?php echo $e->getCategory()->getName(); ?></td>
        <td><?php echo $e->getPlace()->getName(); ?></td>
        <td><?php echo $e->getIssue()->getName();?></td>
        <td><?php echo $e->getIsApproved()?'Yes':'No'; ?></td>
        <td><?php echo $e->getCreatedAt(); ?></td>
        <td><?php echo $e->getUpdatedAt(); ?></td>
        <td>
          <ul class="sf_admin_td_actions">
            <li class="sf_admin_action_edit">
             <a href="<?php echo url_for('complaints/edit?id='.$e->getId())?>">Edit</a>
            </li>
            <li class="sf_admin_action_delete">
            <a href="<?php echo url_for('complaints/delete?id='.$e->getId())?>" onclick="return confirm('Are you sure you want to delete this complaint>.');">Delete</a>
         	</li>
          </ul>
        </td>
      </tr>
      
      <?php endforeach; ?>
    </tbody>
  </table>

</div>