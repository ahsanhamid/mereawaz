<?php use_helper('I18N', 'Date') ?>
<style type="text/css">
	#application_name{
		height:200px;
	}
</style>
<div id="sf_admin_container">
  <h1>Edit Complaint</h1>

  <?php if($sf_user->hasFlash('notice')): ?>
  <div class="notice"><?php echo $sf_user->getFlash('notice'); ?></div>
  <?php endif; ?>

  <div id="sf_admin_header">
      </div>

  <div id="sf_admin_content">
    
<div class="sf_admin_form">
  <form class="form-inline" method="POST" action="<?php echo url_for('complaints/edit?id='.$complaint->getId()); ?>" enctype="multipart/form-data">

	<fieldset id="sf_fieldset_none">
  
                <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_user_inquiry_unique_name">
        <div>	
      <label for="user_inquiry_unique_name">Complaint ID</label>
      <div class="content">
      	<?php  if($form['user_inquiry_unique_name']->hasError()): ?>
            <span class="error"><?php echo $form['user_inquiry_unique_name']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['user_inquiry_unique_name']->render(array('placeholder'=>'Complaint ID')); ?>
      </div>
      

          </div>
  </div>
                <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_issue_desc">
        <div>	
      <label for="issue_desc">Complaint Description</label>
      <div class="content">
      	<?php  if($form['issue_desc']->hasError()): ?>
            <span class="error"><?php echo $form['issue_desc']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['issue_desc']->render(array('placeholder'=>'Complaint Description')); ?>
      </div>
      

          </div>
  </div>
            <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_file_type">
        <div>
      <label for="file_type">File Type</label>
      <div class="content">
      	<?php  if($form['file_type']->hasError()): ?>
            <span class="error"><?php echo $form['file_type']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['file_type']->render(array('placeholder'=>'File Type')); ?>
      </div>

          </div>
  </div>
  <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_file_path">
        <div>
      <label for="file_path">File</label>
      <div class="content">
      	<?php  if($form['file_path']->hasError()): ?>
            <span class="error"><?php echo $form['file_path']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['file_path']->render(array('placeholder'=>'File')); ?>
            <?php if($file_type == "image"){
				if(isset($file_content) && $file_content!= '' ){?>
                	<img src="<?php echo image_path('/uploads/complaints/'.$complaint->getId().'/'.$file_type.'/'.$file_content); ?>" alt="complaint-img"/>
				<?php }else{?>
                	<img src="<?php echo image_path('no-img.png'); ?>" alt="inquery-img"/>
                <?php }?>
            <?php }else if($file_type == "audio"){
				if(isset($file_content) && $file_content!= '' ){?>
                	 <audio controls>
                          <source src="<?php echo image_path('/uploads/complaints/'.$complaint->getId().'/'.$file_type.'/'.$file_content); ?>" type="audio/mpeg">
                     </audio> 
				<?php }else{?>
                	<img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
                <?php }
			}else if($file_type == "video"){
            	if(isset($file_content) && $file_content!= '' ){?>
                      <video width="320" height="240" controls>
                          <source src="<?php echo image_path('/uploads/complaints/'.$complaint->getId().'/'.$file_type.'/'.$file_content); ?>" type="video/mp4">
                      </video> 
				<?php }else{?>
                	<img src="<?php echo image_path('no-img.png'); ?>" alt="no-img" />
                <?php }
			 }?>
      </div>

          </div>
  </div>
  <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_is_approved">
        <div>
      <label for="is_approved">Publish</label>
      <div class="content">
      	<?php  if($form['is_approved']->hasError()): ?>
            <span class="error"><?php echo $form['is_approved']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['is_approved']->render(array('placeholder'=>'Publish')); ?>
      </div>

          </div>
  </div>
      </fieldset>
      <ul class="sf_admin_actions">
		<li class="sf_admin_action_save"><input type="submit" value="Save"></li>
		<li>&nbsp;</li>
        <li class="sf_admin_action_list"><a href="<?php echo url_for('complaints/index');?>">Back to list</a></li>
      </ul>
      <?php echo $form->renderHiddenFields(); ?>
  </form>
</div>
  </div>

  <div id="sf_admin_footer">
      </div>
</div>