<?php

/**
 * app_users actions.
 *
 * @package    mereawaz
 * @subpackage app_users
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class app_usersActions extends sfActions
{
 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function preExecute()
  {
   sfProjectConfiguration::getActive()->loadHelpers(array('I18N'));
   parent::preExecute();
  }

 /**
  * Executes index action
  *
  * @param sfRequest $request A request object
  */
  public function executeIndex(sfWebRequest $request)
  {
    $users = Doctrine_Query::create()
                    ->from('ApplicationUser au')
                    ->execute();
    
    $this->users = $users;
  }
   public function executeNew(sfWebRequest $request)
  {
	$language_array = array("urdu"=>"urdu","english"=>"english");
    $this->form = new sfForm();
    $this->form->setWidgets(array(
        'name' => new sfWidgetFormInputText(),
		'address' => new sfWidgetFormInputText(),
		'email_address' => new sfWidgetFormInputText(),
        'password' => new sfWidgetFormInputPassword(),
        'password_confirm' => new sfWidgetFormInputPassword(),
        'cnic'   => new sfWidgetFormInputText(),
        'phone'  => new sfWidgetFormInputText(),
		'latitude' => new sfWidgetFormInputText(),
		'longitude' => new sfWidgetFormInputText(),
		'token' => new sfWidgetFormInputText(),
		'language' => new sfWidgetFormChoice(array('expanded'=>false,'multiple' => false,'choices'=>$language_array)),
        'is_verified' => new sfWidgetFormChoice(array('choices'=>array('1'=>'Yes','0'=>'No'))),
        ));
    
    $this->form->setValidators(array(
        'name' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'address' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'email_address' => new sfValidatorEmail(array('required'=>true),array('required' => __('Email is required'),'invalid' => __('You must enter a valid email address (me@example.com)'))),
        'password' => new sfValidatorString(array('required'=>false)),
        'password_confirm' => new sfValidatorString(array('required'=>false)),
		'cnic' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'phone' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'latitude' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'longitude' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'token' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
        'language' => new sfValidatorChoice(array('multiple' => false,'choices'=>  array_keys($language_array),'required'=>true),array('required' => __('required'))),
        'is_verified' => new sfValidatorChoice(array('required'=>true,'choices'=>array(1,0)),array('required'=>__('Required'))),

      ));
     $this->form->mergePostValidator(new sfValidatorDoctrineUnique(array('model'=>'sfGuardUser', 'column'=>'email_address'),
                                                                  array('invalid'=>__('Email address already used')))
                                    );
    $this->form->mergePostValidator(new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'password_confirm', array(), array('invalid' => ('The two passwords must be the same.'))));
    if($request->getMethod() == 'POST')
    { 
      $this->form->bind($request->getPostParameters(),$request->getFiles());
      if ($this->form->isValid()) 
      {
        $name = $request->getParameter('name');
		$address = $request->getParameter('address');
		$email_address = $request->getParameter('email_address');
        $password = $request->getParameter('password');
		$cnic = $request->getParameter('cnic');
		$phone = $request->getParameter('phone');
		$latitude = $request->getParameter('latitude');
		$longitude = $request->getParameter('longitude');
        $token = $request->getParameter('token');
        $language = $request->getParameter('language');
        $is_verified = $request->getParameter('is_verified');
        
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();
        
        $sfUser = new sfGuardUser(); 
        $sfUser->setFirstName($name);
        $sfUser->setUsername($email_address);
        $sfUser->setPassword($password);
        $sfUser->setEmailAddress($email_address);
        $sfUser->save();
		$sfUser->addPermissionByName('user');     
        $sfUser->save();
		
		$aUser = new ApplicationUser();
		$aUser->setName($name);
		$aUser->setUserId($sfUser->getId());
		$aUser->setAddress($address);
		$aUser->setCnic($cnic);
		$aUser->setPhone($phone);
		$aUser->setLatitude($latitude);
		$aUser->setLongitude($longitude);
		$aUser->setToken($token);
		$aUser->setLanguage($language);
		$aUser->setIsVerified($is_verified);
		$aUser->save();
		
		$conn->commit();
		
        $this->getUser()->setFlash('notice', 'Application User added successfully');
        $this->redirect('app_users/edit?id='.$aUser->getId());
      }
    }
  }
  public function executeEdit(sfWebRequest $request)
  {
	  $id = $request->getParameter('id');
    $aUser = Doctrine_Query::create()
                       ->from('ApplicationUser au')
                       ->where('au.id = ?',array($id))
                       ->fetchOne();
     if(!$aUser) {
      $this->redirect('@homepage');
    }
    $this->aUser = $aUser;
    $default_array = array();
    $default_array['id'] =  $id;
    $default_array['name'] =  $aUser->getName();
    $default_array['cnic'] =  $aUser->getCnic();
	$default_array['address'] =  $aUser->getAddress();
    $default_array['phone'] =  $aUser->getPhone();
	$default_array['language'] =  $aUser->getLanguage();
    $default_array['is_verified'] =  $aUser->getIsVerified();
	$default_array['latitude'] =  $aUser->getLatitude();
    $default_array['longitude'] =  $aUser->getLongitude();
	$default_array['token'] =  $aUser->getToken();
    $tempStatus = $aUser->getIsVerified();
	if($tempStatus){
		$default_array['is_verified'] =  1;
	}else{
		$default_array['is_verified'] =  0;
	}
    $default_array['email_address'] =  $aUser->getSfGuardUser()->getEmailAddress();
	//print_r($default_array);exit;
	$language_array = array("urdu"=>"urdu","english"=>"english");
    $this->form = new sfForm($default_array);
    $this->form->setWidgets(array(
        'name' => new sfWidgetFormInputText(),
		'address' => new sfWidgetFormInputText(),
		'email_address' => new sfWidgetFormInputText(array('label' => 'Email'),array('readonly'=>true)),
        'password' => new sfWidgetFormInputPassword(),
        'password_confirm' => new sfWidgetFormInputPassword(),
        'cnic'   => new sfWidgetFormInputText(),
        'phone'  => new sfWidgetFormInputText(),
		'latitude' => new sfWidgetFormInputText(),
		'longitude' => new sfWidgetFormInputText(),
		'token' => new sfWidgetFormInputText(),
		'language' => new sfWidgetFormChoice(array('expanded'=>false,'multiple' => false,'choices'=>$language_array)),
        'is_verified' => new sfWidgetFormChoice(array('choices'=>array('1'=>'Yes','0'=>'No'))),
        ));
    
    $this->form->setValidators(array(
        'name' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'address' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'email_address' => new sfValidatorEmail(array('required'=>true),array('required' => __('Email is required'),'invalid' => __('You must enter a valid email address (me@example.com)'))),
        'password' => new sfValidatorString(array('required'=>false)),
        'password_confirm' => new sfValidatorString(array('required'=>false)),
		'cnic' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'phone' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'latitude' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'longitude' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
		'token' => new sfValidatorString(array('required'=>true),array('required'=>__('Required'))),
        'language' => new sfValidatorChoice(array('multiple' => false,'choices'=>  array_keys($language_array),'required'=>true),array('required' => __('required'))),
        'is_verified' => new sfValidatorChoice(array('required'=>true,'choices'=>array(1,0)),array('required'=>__('Required'))),

      ));

    $this->form->mergePostValidator(new sfValidatorSchemaCompare('password', sfValidatorSchemaCompare::EQUAL, 'password_confirm', array(), array('invalid' => ('The two passwords must be the same.'))));
    if($request->getMethod() == 'POST')
    { 
      $this->form->bind($request->getPostParameters(),$request->getFiles());
      if ($this->form->isValid()) 
      {
        $name = $request->getParameter('name');
		$address = $request->getParameter('address');
		$email_address = $request->getParameter('email_address');
        $password = $request->getParameter('password');
		$cnic = $request->getParameter('cnic');
		$phone = $request->getParameter('phone');
		$latitude = $request->getParameter('latitude');
		$longitude = $request->getParameter('longitude');
        $token = $request->getParameter('token');
        $language = $request->getParameter('language');
        $is_verified = $request->getParameter('is_verified');
        
        $conn = Doctrine_Manager::getInstance()->getCurrentConnection();
        $conn->beginTransaction();
        
        //$user = new sfGuardUser();
        $aUser->getSfGuardUser()->setFirstName($name);
		if(isset($password) && $password != ""){
	        $aUser->getSfGuardUser()->setPassword($password);
		}
        $aUser->getSfGuardUser()->save();
		
		$aUser->setName($name);
		$aUser->setAddress($address);
		$aUser->setCnic($cnic);
		$aUser->setPhone($phone);
		$aUser->setLatitude($latitude);
		$aUser->setLongitude($longitude);
		$aUser->setToken($token);
		$aUser->setLanguage($language);
		$aUser->setIsVerified($is_verified);
		$aUser->save();
		
		$conn->commit();
		
        $this->getUser()->setFlash('notice', 'Application User information updated successfully');
        $this->redirect('app_users/edit?id='.$id);
      }
    }
  }
  public function executeDelete(sfWebRequest $request) {
    $id = $request->getParameter('id');
    
    $appComplaints = Doctrine_Query::create()
                       ->from('UserReportedIssue uri')
                       ->where('uri.user_id= ?',array($id))
                       ->execute()
					   ->delete();
    
	$appUser = Doctrine_Query::create()
                       ->from('ApplicationUser au')
                       ->where('au.id = ?',array($id))
                       ->fetchOne();
    if(!$appUser) {
      $this->getUser()->setFlash('notice', __('Not a valid user id!'));
	  $this->redirect('@homepage');
    }
    $appUser->getSfGuardUser()->delete();
    $appUser->delete();
    $this->getUser()->setFlash('notice', __('Deleted!'));
    $this->redirect('app_users/index');
  }
}
