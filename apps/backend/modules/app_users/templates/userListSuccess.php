<?php use_helper('I18N', 'Date') ?>
<style type="text/css">
#sf_admin_container #sf_admin_bar{
	display:block !important;
}
</style>
<div id="push"></div>
<div class="page-header">
  
  
  
  <h1>User list</h1>
  
  
</div>


<div id="sf_admin_container">
<?php if($sf_user->hasFlash('notice')): ?>
  <div class="notice"><?php echo $sf_user->getFlash('notice'); ?></div>
  <?php endif; ?>
</div>





<div class="sf_admin_list">
<table class="table table-bordered table-striped tablesorter stretch" id="sort">
    <thead>
      <tr>
        <th>Name</th>    
        <th>Address</th>
        <th>CNIC</th>
        <th>Is Anonymous</th>
        <th>Created At</th>
        <th>Update At</th>    
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        foreach($users as $e): 
        
      ?>
      <tr id="item-<?php echo $e->getId();?>">
        <td><?php echo $e->getName(); ?></td>
        <td><?php echo $e->getAddress(); ?></td>
        <td><?php echo $e->getCnic(); ?></td>
        <td><?php $tempToken = $e->getToken(); echo (isset($tempToken)?'yes':'no')?></td>
        <td><?php echo $e->getCreatedAt(); ?></td>
        <td><?php echo $e->getUpdatedAt(); ?></td>
        <td>
          <ul class="sf_admin_td_actions">
            <li class="sf_admin_action_edit">
             
            </li>
            <li class="sf_admin_action_delete">
            
             
         	</li>
          </ul>
        </td>
      </tr>
      
      <?php endforeach; ?>
    </tbody>
  </table>

</div>

<ul class="sf_admin_actions">
      <li class="sf_admin_action_new"></li>    
</ul>
      
<?php if($pager->haveToPaginate()): ?>
  <div class="paging">
    <?php echo generate_pagination_links($pager,'app_users/userList',$sf_request->getParameter('page','1')); ?>
  </div>
<?php endif; ?>