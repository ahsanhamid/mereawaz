<?php use_helper('I18N', 'Date') ?>
<style type="text/css">
	#application_name{
		height:200px;
	}
</style>
<div id="sf_admin_container">
  <h1>Edit Application User</h1>

  <?php if($sf_user->hasFlash('notice')): ?>
  <div class="notice"><?php echo $sf_user->getFlash('notice'); ?></div>
  <?php endif; ?>

  <div id="sf_admin_header">
      </div>

  <div id="sf_admin_content">
    
<div class="sf_admin_form">
  <form class="form-inline" method="POST" action="<?php echo url_for('app_users/edit?id='.$aUser->getId()); ?>">

	<fieldset id="sf_fieldset_none">
  
                <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_name">
        <div>	
      <label for="name">Name</label>
      <div class="content">
      	<?php  if($form['name']->hasError()): ?>
            <span class="error"><?php echo $form['name']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['name']->render(array('placeholder'=>'Name')); ?>
      </div>
      

          </div>
  </div>
                <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_address">
        <div>	
      <label for="address">Address</label>
      <div class="content">
      	<?php  if($form['address']->hasError()): ?>
            <span class="error"><?php echo $form['address']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['address']->render(array('placeholder'=>'Address')); ?>
      </div>
      

          </div>
  </div>
            <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_cnic">
        <div>
      <label for="cnic">CNIC</label>
      <div class="content">
      	<?php  if($form['cnic']->hasError()): ?>
            <span class="error"><?php echo $form['cnic']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['cnic']->render(array('placeholder'=>'CNIC')); ?>
      </div>

          </div>
  </div>
  <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_email_address">
        <div>
      <label for="email_address">Email address</label>
      <div class="content">
      	<?php  if($form['email_address']->hasError()): ?>
            <span class="error"><?php echo $form['email_address']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['email_address']->render(array('placeholder'=>'Email address')); ?>
      </div>

          </div>
  </div>
  <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_password">
        <div>
      <label for="password">Password</label>
      <div class="content">
      	<?php  if($form['password']->hasError()): ?>
            <span class="error"><?php echo $form['password']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['password']->render(array('placeholder'=>'Password')); ?>
      </div>

          </div>
  </div>
  <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_password_confirm">
        <div>
      <label for="password_confirm">Confirm Password</label>
      <div class="content">
      	<?php  if($form['password_confirm']->hasError()): ?>
            <span class="error"><?php echo $form['password_confirm']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['password_confirm']->render(array('placeholder'=>'Confirm Password')); ?>
      </div>

          </div>
  </div>
    <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_latitude">
        <div>
      <label for="latitude">Latitude</label>
      <div class="content">
      	<?php  if($form['latitude']->hasError()): ?>
            <span class="error"><?php echo $form['latitude']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['latitude']->render(array('placeholder'=>'Latitude')); ?>
      </div>

          </div>
  </div>
    <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_longitude">
        <div>
      <label for="longitude">Longitude</label>
      <div class="content">
      	<?php  if($form['longitude']->hasError()): ?>
            <span class="error"><?php echo $form['longitude']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['longitude']->render(array('placeholder'=>'Longitude')); ?>
      </div>

          </div>
  </div>
  <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_phone">
        <div>
      <label for="phone">Phone</label>
      <div class="content">
      	<?php  if($form['phone']->hasError()): ?>
            <span class="error"><?php echo $form['phone']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['phone']->render(array('placeholder'=>'Phone')); ?>
      </div>

          </div>
  </div>
    <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_token">
        <div>
      <label for="token">Token</label>
      <div class="content">
      	<?php  if($form['token']->hasError()): ?>
            <span class="error"><?php echo $form['token']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['token']->render(array('placeholder'=>'Token')); ?>
      </div>

          </div>
  </div>
    <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_language">
        <div>
      <label for="language">Language</label>
      <div class="content">
      	<?php  if($form['language']->hasError()): ?>
            <span class="error"><?php echo $form['language']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['language']->render(array('placeholder'=>'Language')); ?>
      </div>

          </div>
  </div>
      <div class="sf_admin_form_row sf_admin_text sf_admin_form_field_is_verified">
        <div>
      <label for="is_verified">Is Verified</label>
      <div class="content">
      	<?php  if($form['is_verified']->hasError()): ?>
            <span class="error"><?php echo $form['is_verified']->renderError(); ?></span>
			<?php endif;  ?>
            <?php echo $form['is_verified']->render(array('placeholder'=>'Is Verified')); ?>
      </div>

          </div>
  </div>
      </fieldset>
      <ul class="sf_admin_actions">
		<li class="sf_admin_action_save"><input type="submit" value="Save"></li>
		<li>&nbsp;</li>
        <li class="sf_admin_action_list"><a href="<?php echo url_for('app_users/index');?>">Back to list</a></li>
      </ul>
      <?php echo $form->renderHiddenFields(); ?>
  </form>
</div>
  </div>

  <div id="sf_admin_footer">
      </div>
</div>