<?php use_helper('I18N', 'Date') ?>
<style type="text/css">
#sf_admin_container #sf_admin_bar{
	display:block !important;
}
</style>
<div id="push"></div>
<div class="page-header">
  
  
  
  <h1>User list</h1>
  
  
</div>


<div id="sf_admin_container">
<?php if($sf_user->hasFlash('notice')): ?>
  <div class="notice"><?php echo $sf_user->getFlash('notice'); ?></div>
  <?php endif; ?>
</div>





<div class="sf_admin_list">
<table class="table table-bordered table-striped tablesorter stretch" id="sort">
    <thead>
      <tr>
        <th>Name</th>    
        <th>Email address</th>
        <th>Is Verified</th>
        <th>Is Anonymous</th>
        <th>Created At</th>
        <th>Update At</th>    
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      <?php 
        foreach($users as $e): 
        
      ?>
      <tr id="item-<?php echo $e->getId();?>">
        <td><?php echo $e->getName(); ?></td>
        <td><?php $tempToken = $e->getToken(); $tempEmail = $e->getSfGuardUser()->getEmailAddress(); echo (($tempToken == $tempEmail)?'Anonymous':$tempEmail); ?></td>
        <td><?php echo $e->getIsVerified()?'Yes':'No'; ?></td>
        <td><?php $tempToken = $e->getToken(); $tempEmail = $e->getSfGuardUser()->getEmailAddress(); echo (($tempToken == $tempEmail)?'yes':'no')?></td>
        <td><?php echo $e->getCreatedAt(); ?></td>
        <td><?php echo $e->getUpdatedAt(); ?></td>
        <td>
          <ul class="sf_admin_td_actions">
            <li class="sf_admin_action_edit">
             <a href="<?php echo url_for('app_users/edit?id='.$e->getId())?>">Edit</a>
            </li>
            <li class="sf_admin_action_delete">
            <a href="<?php echo url_for('app_users/delete?id='.$e->getId())?>" onclick="return confirm('Are you sure you want to delete this user? All of his complaints will be deleted too.');">Delete</a>
         	</li>
          </ul>
        </td>
      </tr>
      
      <?php endforeach; ?>
    </tbody>
  </table>

</div>

<ul class="sf_admin_actions">
      <li class="sf_admin_action_new"><a href="<?php echo url_for('app_users/new');?>">New</a></li>    
</ul>