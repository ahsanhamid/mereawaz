<?php use_helper('I18N') ?>

<form action="<?php echo url_for('@sf_guard_signin') ?>" method="post" class="form-signin">
    <h2 class="form-signin-heading">Mereawaz</h2>
    <div id="login_panel">
      <div class="login_fields">
        <div class="field">
          <?php echo $form['username']->renderRow(array('placeholder'=>'Username or E-Mail')) ?>
        </div>

        <div class="field">
          <?php echo $form['password']->renderRow(array('placeholder'=>'Password')) ?>		
        </div>
        
        <label class="checkbox">
          <?php echo $form['remember']->render() ?> Remember me
        </label>

      </div> <!-- .login_fields -->

      <div class="login_actions">
        <?php echo $form['_csrf_token']->render() ?>		
        <button class="btn btn-large btn-primary" type="submit" tabindex="3"><?php echo __('Signin', null, 'sf_guard') ?></button>
      </div>
    </div> <!-- #login_panel -->		
</form>

