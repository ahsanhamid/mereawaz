<?php

require_once dirname(__FILE__).'/../lib/sfGuardUserGeneratorConfiguration.class.php';
require_once dirname(__FILE__).'/../lib/sfGuardUserGeneratorHelper.class.php';

/**
 * sfGuardUser actions.
 *
 * @package    sfGuardPlugin
 * @subpackage sfGuardUser
 * @author     Fabien Potencier
 * @version    SVN: $Id: actions.class.php 23319 2009-10-25 12:22:23Z Kris.Wallsmith $
 */
class sfGuardUserActions extends autoSfGuardUserActions
{
  public function executeExportUsers(sfWebRequest $request){
	  $this->setLayout(false);
	sfConfig::set('sf_web_debug', false);
	
	$users = Doctrine_Query::create()
                ->from('User c, c.sfGuardUser u')
                ->execute();
	$user_info = array();
	$user_info_str = '';
	$out = fopen('php://output', 'w');
	fputcsv($out, array("Email Address","Username","Role","Getmetocollege Credit","Referred By","User Package"));
	if($users){
		foreach($users as $user){
			$u_info = array();
			$u_info[] = $user->getEmailAddr();
			$u_info[] = $user->getUserLogin();
			$u_info[] = $user->getUserRole();
			$u_info[] = $user->getUserCredit();
			$u_info[] = $user->getReferredUserEmail();//$user->getReferredBy();
			$u_info[] = $user->getUserPackage();
			//$user_info_str = implode(",",$u_info);
			fputcsv($out, $u_info);
		}
	}
	fclose($out);
	$this->getResponse()->setHttpHeader('Content-Type', 'application/csv');
	$this->getResponse()->setHttpHeader('Content-Disposition', 'attachment; filename="file.csv"');
	$this->getResponse()->setHttpHeader('Pragma', 'no-cache');
	$this->getResponse()->setHttpHeader('Expires', '0');
	return sfView::NONE;
  }
}
