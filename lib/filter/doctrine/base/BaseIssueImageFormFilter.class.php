<?php

/**
 * IssueImage filter form base class.
 *
 * @package    mereawaz
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseIssueImageFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'issue_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('UserReportedIssue'), 'add_empty' => true)),
      'file_type'  => new sfWidgetFormChoice(array('choices' => array('' => '', 'image' => 'image', 'audio' => 'audio', 'video' => 'video'))),
      'file_path'  => new sfWidgetFormFilterInput(),
      'is_cover'   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at' => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'issue_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('UserReportedIssue'), 'column' => 'id')),
      'file_type'  => new sfValidatorChoice(array('required' => false, 'choices' => array('image' => 'image', 'audio' => 'audio', 'video' => 'video'))),
      'file_path'  => new sfValidatorPass(array('required' => false)),
      'is_cover'   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at' => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('issue_image_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IssueImage';
  }

  public function getFields()
  {
    return array(
      'id'         => 'Number',
      'issue_id'   => 'ForeignKey',
      'file_type'  => 'Enum',
      'file_path'  => 'Text',
      'is_cover'   => 'Boolean',
      'created_at' => 'Date',
      'updated_at' => 'Date',
    );
  }
}
