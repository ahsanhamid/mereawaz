<?php

/**
 * UserReportedIssue filter form base class.
 *
 * @package    mereawaz
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseUserReportedIssueFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'user_id'                  => new sfWidgetFormFilterInput(),
      'user_inquiry_unique_name' => new sfWidgetFormFilterInput(),
      'cat_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Category'), 'add_empty' => true)),
      'place_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Place'), 'add_empty' => true)),
      'issue_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Issue'), 'add_empty' => true)),
      'city_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('City'), 'add_empty' => true)),
      'city_name'                => new sfWidgetFormFilterInput(),
      'province_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Province'), 'add_empty' => true)),
      'issue_name'               => new sfWidgetFormFilterInput(),
      'issue_desc'               => new sfWidgetFormFilterInput(),
      'file_type'                => new sfWidgetFormChoice(array('choices' => array('' => '', 'image' => 'image', 'audio' => 'audio', 'video' => 'video'))),
      'file_path'                => new sfWidgetFormFilterInput(),
      'victim_name'              => new sfWidgetFormFilterInput(),
      'victim_address'           => new sfWidgetFormFilterInput(),
      'victim_age'               => new sfWidgetFormFilterInput(),
      'oversea_name'             => new sfWidgetFormFilterInput(),
      'oversea_phone'            => new sfWidgetFormFilterInput(),
      'oversea_consulate'        => new sfWidgetFormFilterInput(),
      'list_order'               => new sfWidgetFormFilterInput(),
      'is_approved'              => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'longitude'                => new sfWidgetFormFilterInput(),
      'latitude'                 => new sfWidgetFormFilterInput(),
      'created_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
      'updated_at'               => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate(), 'with_empty' => false)),
    ));

    $this->setValidators(array(
      'user_id'                  => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'user_inquiry_unique_name' => new sfValidatorPass(array('required' => false)),
      'cat_id'                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Category'), 'column' => 'id')),
      'place_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Place'), 'column' => 'id')),
      'issue_id'                 => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Issue'), 'column' => 'id')),
      'city_id'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('City'), 'column' => 'id')),
      'city_name'                => new sfValidatorPass(array('required' => false)),
      'province_id'              => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Province'), 'column' => 'id')),
      'issue_name'               => new sfValidatorPass(array('required' => false)),
      'issue_desc'               => new sfValidatorPass(array('required' => false)),
      'file_type'                => new sfValidatorChoice(array('required' => false, 'choices' => array('image' => 'image', 'audio' => 'audio', 'video' => 'video'))),
      'file_path'                => new sfValidatorPass(array('required' => false)),
      'victim_name'              => new sfValidatorPass(array('required' => false)),
      'victim_address'           => new sfValidatorPass(array('required' => false)),
      'victim_age'               => new sfValidatorPass(array('required' => false)),
      'oversea_name'             => new sfValidatorPass(array('required' => false)),
      'oversea_phone'            => new sfValidatorPass(array('required' => false)),
      'oversea_consulate'        => new sfValidatorPass(array('required' => false)),
      'list_order'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_approved'              => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'longitude'                => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'latitude'                 => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'               => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('user_reported_issue_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserReportedIssue';
  }

  public function getFields()
  {
    return array(
      'id'                       => 'Number',
      'user_id'                  => 'Number',
      'user_inquiry_unique_name' => 'Text',
      'cat_id'                   => 'ForeignKey',
      'place_id'                 => 'ForeignKey',
      'issue_id'                 => 'ForeignKey',
      'city_id'                  => 'ForeignKey',
      'city_name'                => 'Text',
      'province_id'              => 'ForeignKey',
      'issue_name'               => 'Text',
      'issue_desc'               => 'Text',
      'file_type'                => 'Enum',
      'file_path'                => 'Text',
      'victim_name'              => 'Text',
      'victim_address'           => 'Text',
      'victim_age'               => 'Text',
      'oversea_name'             => 'Text',
      'oversea_phone'            => 'Text',
      'oversea_consulate'        => 'Text',
      'list_order'               => 'Number',
      'is_approved'              => 'Boolean',
      'longitude'                => 'Number',
      'latitude'                 => 'Number',
      'created_at'               => 'Date',
      'updated_at'               => 'Date',
    );
  }
}
