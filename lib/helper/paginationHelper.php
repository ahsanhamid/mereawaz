<?php
function generate_pagination_links_custom($users_counter, $users,$url,$current_page = 1,$chapter_title='',$book='',$section='')
{
  $url = $url;
  $rpp = 20;
  $pages = ceil(count($users_counter)/$rpp);
  $html = '';
  if ($pages > 0)
  {
    $html .= '<div class="pagination"><span>Total '.count($users_counter).' found</span><ul>';
    
    if($current_page != 1)
    {
      //echo '<li>'.link_to('First', $url.'?page='.$pager->getFirstPage()).'</li>';
      $html .=  '<li class="prev">'.link_to('Prev', $url,array('query_string'=>'page='.($current_page-1).'&chapter_title='.$chapter_title.'&book='.$book.'&section='.$section)).'</li>';
    }
    else
    {
      $html .=  '<li class="prev">'.link_to('Prev', $url,array('query_string'=>'page='.$pages.'&chapter_title='.$chapter_title.'&book='.$book.'&section='.$section)).'</li>';
    }
    if($pages > 1){
		$counter = 0;
		$temp_start_page_counter = 1;
		$remaining_pages_to_traverse = 0;
		if($current_page != 1){
			$remaining_pages_to_traverse = $pages - $current_page;
			//echo "r p = ".$remaining_pages_to_traverse;exit;
			if($remaining_pages_to_traverse >= 0 && $pages > 3){
				$temp_start_page_counter = $current_page-1;
			}else{
				$temp_start_page_counter = 1;
			}
		}
		for($i=$temp_start_page_counter;$i<=$pages;$i++)
		{
			if($counter < 3){
			  if($current_page==$i)
			  {
				$html .=  '<li class="active">'.link_to($i,$url,array('query_string'=>'page='.$i.'&chapter_title='.$chapter_title.'&book='.$book.'&section='.$section)).'</li>';
			  }
			  else
			  {
				$html .=  '<li>'.link_to($i,$url,array('query_string'=>'page='.$i.'&chapter_title='.$chapter_title.'&book='.$book.'&section='.$section)).'</li>';
			  }
			}
			$counter++;
		}
	}
    if($current_page != $pages)
    {
      $html .=  '<li class="next">'.link_to('Next', $url,array('query_string'=>'page='.($current_page+1).'&chapter_title='.$chapter_title.'&book='.$book.'&section='.$section)).'</li>';
    
      //echo '<li>'.link_to('Last', $url.'?page='.$pager->getLastPage()).'</li>';
    }
    else
    {
      $html .=  '<li class="next">'.link_to('Next', $url,array('query_string'=>'page=1'.'&chapter_title='.$chapter_title.'&book='.$book.'&section='.$section)).'</li>';
    }
    //$html .=  '</ul><a href="#">New User</a></div>';
  }
  return $html;
}

function generate_pagination_links($pager,$url,$current_page = 1)
{
  $url = $url;
  $pagerRange = $pager->getRange('Sliding',array('chunk' => 10));
  $pages = $pagerRange->rangeAroundPage();
  
  $html = '';
  if ($pager->haveToPaginate())
  {
    $html .= '<div class="pagination"><ul>';
    
    if($current_page != $pager->getFirstPage())
    {
      //echo '<li>'.link_to('First', $url.'?page='.$pager->getFirstPage()).'</li>';
      $html .=  '<li class="prev">'.link_to('Prev', $url,array('query_string'=>'page='.$pager->getPreviousPage())).'</li>';
    }
    else
    {
      $html .=  '<li class="prev">'.link_to('Prev', $url,array('query_string'=>'page='.$pager->getFirstPage())).'</li>';
    }
    
    $pages = $pages->getRawValue();
    foreach($pages as $page)
    {
      if($current_page==$page)
      {
        $html .=  '<li class="active">'.link_to($page,$url,array('query_string'=>'page='.$page)).'</li>';
      }
      else
      {
        $html .=  '<li>'.link_to($page,$url,array('query_string'=>'page='.$page)).'</li>';
      }
    }

    if($current_page != $pager->getLastPage())
    {
      $html .=  '<li class="next">'.link_to('Next', $url,array('query_string'=>'page='.$pager->getNextPage())).'</li>';
    
      //echo '<li>'.link_to('Last', $url.'?page='.$pager->getLastPage()).'</li>';
    }
    else
    {
      $html .=  '<li class="next">'.link_to('Next', $url,array('query_string'=>'page='.$pager->getLastPage())).'</li>';
    }
    $html .=  '</ul></div>';
  }
  return $html;
}
function generate_pagination_links_lesson($pager,$url,$current_page = 1,$lesson_title='',$chapter='')
{
  $url = $url;
  $pagerRange = $pager->getRange('Sliding',array('chunk' => 10));
  $pages = $pagerRange->rangeAroundPage();
  
  $html = '';
  if ($pager->haveToPaginate())
  {
    $html .= '<div class="pagination"><ul>';
    
    if($current_page != $pager->getFirstPage())
    {
      //echo '<li>'.link_to('First', $url.'?page='.$pager->getFirstPage()).'</li>';
      $html .=  '<li class="prev">'.link_to('Prev', $url,array('query_string'=>'page='.$pager->getPreviousPage()."&lesson_title=".$lesson_title."&chapter=".$chapter)).'</li>';
    }
    else
    {
      $html .=  '<li class="prev">'.link_to('Prev', $url,array('query_string'=>'page='.$pager->getFirstPage()."&lesson_title=".$lesson_title."&chapter=".$chapter)).'</li>';
    }
    
    $pages = $pages->getRawValue();
    foreach($pages as $page)
    {
      if($current_page==$page)
      {
        $html .=  '<li class="active">'.link_to($page,$url,array('query_string'=>'page='.$page."&lesson_title=".$lesson_title."&chapter=".$chapter)).'</li>';
      }
      else
      {
        $html .=  '<li>'.link_to($page,$url,array('query_string'=>'page='.$page."&lesson_title=".$lesson_title."&chapter=".$chapter)).'</li>';
      }
    }

    if($current_page != $pager->getLastPage())
    {
      $html .=  '<li class="next">'.link_to('Next', $url,array('query_string'=>'page='.$pager->getNextPage()."&lesson_title=".$lesson_title."&chapter=".$chapter)).'</li>';
    
      //echo '<li>'.link_to('Last', $url.'?page='.$pager->getLastPage()).'</li>';
    }
    else
    {
      $html .=  '<li class="next">'.link_to('Next', $url,array('query_string'=>'page='.$pager->getLastPage()."&lesson_title=".$lesson_title."&chapter=".$chapter)).'</li>';
    }
    $html .=  '</ul></div>';
  }
  return $html;
}


?>
