<?php
/**
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class Version10 extends Doctrine_Migration_Base
{
    public function up()
    {
        $this->createTable('application_user', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'user_id' => 
             array(
              'type' => 'integer',
              'notnull' => '',
              'length' => '5',
             ),
             'member_id' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'address' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'cnic' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'phone' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'language' => 
             array(
              'type' => 'enum',
              'values' => 
              array(
              0 => 'english',
              1 => 'urdu',
              ),
              'length' => '',
             ),
             'is_verified' => 
             array(
              'type' => 'boolean',
              'default' => '0',
              'length' => '25',
             ),
             'longitude' => 
             array(
              'type' => 'decimal',
              'scale' => '15',
              'notnull' => '',
              'length' => '18',
             ),
             'latitude' => 
             array(
              'type' => 'decimal',
              'scale' => '15',
              'notnull' => '',
              'length' => '18',
             ),
             'token' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->createTable('category', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'description' => 
             array(
              'type' => 'clob',
              'length' => '',
             ),
             'status' => 
             array(
              'type' => 'boolean',
              'default' => '1',
              'length' => '25',
             ),
             'logo' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->createTable('category_place', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'cat_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'place_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             ));
        $this->createTable('city', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->createTable('issue', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'description' => 
             array(
              'type' => 'clob',
              'length' => '',
             ),
             'status' => 
             array(
              'type' => 'boolean',
              'default' => '1',
              'length' => '25',
             ),
             'logo' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->createTable('place', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'description' => 
             array(
              'type' => 'clob',
              'length' => '',
             ),
             'status' => 
             array(
              'type' => 'boolean',
              'default' => '1',
              'length' => '25',
             ),
             'logo' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->createTable('place_issue', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'place_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'issue_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             ));
        $this->createTable('province', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'name' => 
             array(
              'type' => 'string',
              'length' => '255',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
        $this->createTable('user_reported_issue', array(
             'id' => 
             array(
              'type' => 'integer',
              'primary' => '1',
              'autoincrement' => '1',
              'length' => '5',
             ),
             'user_id' => 
             array(
              'type' => 'integer',
              'notnull' => '',
              'length' => '5',
             ),
             'cat_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'place_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'issue_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'city_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'province_id' => 
             array(
              'type' => 'integer',
              'length' => '5',
             ),
             'issue_name' => 
             array(
              'type' => 'string',
              'length' => '500',
             ),
             'issue_desc' => 
             array(
              'type' => 'clob',
              'length' => '',
             ),
             'file_type' => 
             array(
              'type' => 'enum',
              'values' => 
              array(
              0 => 'image',
              1 => 'audio',
              2 => 'video',
              ),
              'length' => '',
             ),
             'file_path' => 
             array(
              'type' => 'string',
              'length' => '500',
             ),
             'created_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             'updated_at' => 
             array(
              'notnull' => '1',
              'type' => 'timestamp',
              'length' => '25',
             ),
             ), array(
             'primary' => 
             array(
              0 => 'id',
             ),
             'collate' => 'utf8_unicode_ci',
             'charset' => 'utf8',
             ));
    }

    public function down()
    {
        $this->dropTable('application_user');
        $this->dropTable('category');
        $this->dropTable('category_place');
        $this->dropTable('city');
        $this->dropTable('issue');
        $this->dropTable('place');
        $this->dropTable('place_issue');
        $this->dropTable('province');
        $this->dropTable('user_reported_issue');
    }
}