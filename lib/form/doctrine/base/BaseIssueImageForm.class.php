<?php

/**
 * IssueImage form base class.
 *
 * @method IssueImage getObject() Returns the current form's model object
 *
 * @package    mereawaz
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIssueImageForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'         => new sfWidgetFormInputHidden(),
      'issue_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('UserReportedIssue'), 'add_empty' => true)),
      'file_type'  => new sfWidgetFormChoice(array('choices' => array('image' => 'image', 'audio' => 'audio', 'video' => 'video'))),
      'file_path'  => new sfWidgetFormTextarea(),
      'is_cover'   => new sfWidgetFormInputCheckbox(),
      'created_at' => new sfWidgetFormDateTime(),
      'updated_at' => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'issue_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('UserReportedIssue'), 'required' => false)),
      'file_type'  => new sfValidatorChoice(array('choices' => array(0 => 'image', 1 => 'audio', 2 => 'video'), 'required' => false)),
      'file_path'  => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'is_cover'   => new sfValidatorBoolean(array('required' => false)),
      'created_at' => new sfValidatorDateTime(),
      'updated_at' => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('issue_image[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IssueImage';
  }

}
