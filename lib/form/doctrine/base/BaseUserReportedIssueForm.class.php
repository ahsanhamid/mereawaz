<?php

/**
 * UserReportedIssue form base class.
 *
 * @method UserReportedIssue getObject() Returns the current form's model object
 *
 * @package    mereawaz
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseUserReportedIssueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'user_id'                  => new sfWidgetFormInputText(),
      'user_inquiry_unique_name' => new sfWidgetFormInputText(),
      'cat_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Category'), 'add_empty' => true)),
      'place_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Place'), 'add_empty' => true)),
      'issue_id'                 => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Issue'), 'add_empty' => true)),
      'city_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('City'), 'add_empty' => true)),
      'city_name'                => new sfWidgetFormInputText(),
      'province_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Province'), 'add_empty' => true)),
      'issue_name'               => new sfWidgetFormTextarea(),
      'issue_desc'               => new sfWidgetFormTextarea(),
      'file_type'                => new sfWidgetFormChoice(array('choices' => array('image' => 'image', 'audio' => 'audio', 'video' => 'video'))),
      'file_path'                => new sfWidgetFormTextarea(),
      'victim_name'              => new sfWidgetFormInputText(),
      'victim_address'           => new sfWidgetFormInputText(),
      'victim_age'               => new sfWidgetFormInputText(),
      'oversea_name'             => new sfWidgetFormInputText(),
      'oversea_phone'            => new sfWidgetFormInputText(),
      'oversea_consulate'        => new sfWidgetFormInputText(),
      'list_order'               => new sfWidgetFormInputText(),
      'is_approved'              => new sfWidgetFormInputCheckbox(),
      'longitude'                => new sfWidgetFormInputText(),
      'latitude'                 => new sfWidgetFormInputText(),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'                  => new sfValidatorInteger(array('required' => false)),
      'user_inquiry_unique_name' => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'cat_id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Category'), 'required' => false)),
      'place_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Place'), 'required' => false)),
      'issue_id'                 => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Issue'), 'required' => false)),
      'city_id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('City'), 'required' => false)),
      'city_name'                => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'province_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Province'), 'required' => false)),
      'issue_name'               => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'issue_desc'               => new sfValidatorString(array('required' => false)),
      'file_type'                => new sfValidatorChoice(array('choices' => array(0 => 'image', 1 => 'audio', 2 => 'video'), 'required' => false)),
      'file_path'                => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'victim_name'              => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'victim_address'           => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'victim_age'               => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'oversea_name'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'oversea_phone'            => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'oversea_consulate'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'list_order'               => new sfValidatorInteger(array('required' => false)),
      'is_approved'              => new sfValidatorBoolean(array('required' => false)),
      'longitude'                => new sfValidatorNumber(array('required' => false)),
      'latitude'                 => new sfValidatorNumber(array('required' => false)),
      'created_at'               => new sfValidatorDateTime(),
      'updated_at'               => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('user_reported_issue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'UserReportedIssue';
  }

}
