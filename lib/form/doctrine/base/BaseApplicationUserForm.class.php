<?php

/**
 * ApplicationUser form base class.
 *
 * @method ApplicationUser getObject() Returns the current form's model object
 *
 * @package    mereawaz
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseApplicationUserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'user_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'add_empty' => true)),
      'name'        => new sfWidgetFormInputText(),
      'address'     => new sfWidgetFormInputText(),
      'cnic'        => new sfWidgetFormInputText(),
      'phone'       => new sfWidgetFormInputText(),
      'language'    => new sfWidgetFormChoice(array('choices' => array('english' => 'english', 'urdu' => 'urdu'))),
      'is_verified' => new sfWidgetFormInputCheckbox(),
      'longitude'   => new sfWidgetFormInputText(),
      'latitude'    => new sfWidgetFormInputText(),
      'token'       => new sfWidgetFormInputText(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'user_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('sfGuardUser'), 'required' => false)),
      'name'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'address'     => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'cnic'        => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'phone'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'language'    => new sfValidatorChoice(array('choices' => array(0 => 'english', 1 => 'urdu'), 'required' => false)),
      'is_verified' => new sfValidatorBoolean(array('required' => false)),
      'longitude'   => new sfValidatorNumber(array('required' => false)),
      'latitude'    => new sfValidatorNumber(array('required' => false)),
      'token'       => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'created_at'  => new sfValidatorDateTime(),
      'updated_at'  => new sfValidatorDateTime(),
    ));

    $this->widgetSchema->setNameFormat('application_user[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'ApplicationUser';
  }

}
